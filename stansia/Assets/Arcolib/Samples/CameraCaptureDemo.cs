﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Vexpot.Arcolib.Integration;

namespace Vexpot.Arcolib.Samples
{
    /// <summary>
    /// Demo to show how to use <see cref="CameraCapture"/> class.
    /// </summary>
    public class CameraCaptureDemo : MonoBehaviour
    {
        /// <summary>
        /// 
        /// </summary>
        public MeshRenderer canvas;

        /// <summary>
        /// 
        /// </summary>
        public Slider zoomControl;
        /// <summary>
        /// 
        /// </summary>
        public Button torchControl;
        /// <summary>
        /// 
        /// </summary>
        public Button whiteBalanceControl;
        /// <summary>
        /// 
        /// </summary>
        public Button exposureControl;

        /// <summary>
        /// 
        /// </summary>
        public Button focusControl;

        /// <summary>
        /// 
        /// </summary>
        public uint deviceIndex = 0;

        /// <summary>
        /// 
        /// </summary>
        public uint captureWidth = 640;

        /// <summary>
        /// 
        /// </summary>
        public uint captureHeight = 480;

        /// <summary>
        /// 
        /// </summary>
        private Texture2D _texture;

        /// <summary>
        /// 
        /// </summary>
        private CameraCapture _capture;

        // Use this for initialization
        void Start()
        {
            if (deviceIndex < CameraCapture.camerasCount)
            {
                _capture = new CameraCapture();
                _capture.Open(deviceIndex, captureWidth, captureHeight);

#if UNITY_EDITOR
                _capture.horizontalFlip = true;
#endif
                setupZoom();

                if (whiteBalanceControl)
                {
                    whiteBalanceControl.interactable = _capture.IsWhiteBalanceModeSupported(WhiteBalanceMode.ContinuousAuto);
                }

                if (exposureControl)
                {
                    exposureControl.interactable = _capture.IsExposureModeSupported(ExposureMode.ContinuousAuto);
                }

                if (torchControl)
                {
                    torchControl.interactable = _capture.isTorchAvailable;
                }

                if (focusControl)
                {
                    focusControl.interactable = _capture.IsFocusModeSupported(FocusMode.ContinuousAuto);
                }

                _texture = new Texture2D(1, 1, TextureFormat.RGB24, false);

                if (canvas)
                    canvas.material.mainTexture = _texture;
            }
        }

        /// <summary>
        /// Takes a picture from capture.
        /// </summary>
        public void takePicture()
        {
            if (_texture)
                File.WriteAllBytes("image.jpg", _texture.EncodeToJPG());
        }

        /// <summary>
        /// 
        /// </summary>
        public void toogleExposure()
        {
            if (_capture != null)
            {
                _capture.exposureMode = (_capture.exposureMode == ExposureMode.ContinuousAuto ?
                ExposureMode.Locked :
                ExposureMode.ContinuousAuto);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void toogleWhiteBalance()
        {
            if (_capture != null)
            {
                _capture.whiteBalanceMode = (_capture.whiteBalanceMode == WhiteBalanceMode.ContinuousAuto ?
                WhiteBalanceMode.Locked :
                WhiteBalanceMode.ContinuousAuto);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void toogleTorchMode()
        {
            if (_capture != null && _capture.isTorchAvailable)
            {
                _capture.torchMode = (_capture.torchMode == TorchMode.Off ?
                TorchMode.On :
                TorchMode.Off);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void toogleAutoFocus()
        {
            if (_capture != null)
            {
                _capture.focusMode = (_capture.focusMode == FocusMode.ContinuousAuto ?
                    FocusMode.Locked :
                    FocusMode.ContinuousAuto);
            }
        }

        void setupZoom()
        {
            if (zoomControl && _capture != null)
            {
                if (_capture.isZoomSupported)
                {
                    zoomControl.maxValue = _capture.maxZoom;
                    zoomControl.minValue = _capture.minZoom;

                    zoomControl.onValueChanged.AddListener(value =>
                    {
                        _capture.zoom = value;
                    });
                }
                else
                {
                    zoomControl.gameObject.SetActive(false);
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (_capture != null)
            {
                _capture.ReadFrame(_texture);
            }
        }

        void OnDestroy()
        {
            if (_capture != null)
                _capture.Close();
        }
    }
}
