﻿using System;
using UnityEngine;

namespace Vexpot.Arcolib.Samples
{
    /// <summary>
    /// This is a simple demo showing the basics of natural image detection on Arcolib.    
    /// </summary>
    public class SimpleFeaturesSetup : MonoBehaviour
    {
        /// <summary>
        /// Pre-built FeaturesModel. 
        /// </summary>
        public TextAsset trainingData;

        /// <summary>
        /// Static image to be used as input source. See <see cref="Texture2DInput"/> for more details.
        /// </summary>
        public Texture2D inputImage;

        /// <summary>
        ///  This is a tracker specialized in natural images detection and tracking.
        ///  See <see cref="FeaturesTracker"/> and <see cref="FeaturesModel"/> for more
        ///  details.
        /// </summary>
        FeaturesTracker _tracker;

        /// <summary>
        ///  The training model. Used by trackers to learn what to find.
        /// </summary>
        FeaturesModel _dataModel;

        /// <summary>
        /// The resulting frame emitted by tracker. All results are stored here.
        /// See <see cref="FeaturesSymbolFrame"/> for more details.
        /// </summary>
        FeaturesSymbolFrame _result;

        /// <summary>
        /// The generic device calibration (camera, image, and others).
        /// See <see cref="DeviceCalibration"/> for more details.
        /// </summary>
        DeviceCalibration _calibration;

        /// <summary>
        ///  The tracker's options. Multiple trackers can share the same options object.
        ///  Options are applied only when tracker starts, if you change something in the options you need 
        ///  to Stop/Start the tracker to see new options taking effect. See <see cref="TrackerOptions"/>
        ///  for more details.
        /// </summary>
        TrackerOptions _options;

        /// <summary>
        ///  The tracker's input source. See <see cref="InputSource"/> for more details.
        /// </summary>
        InputSource _input;

        /// <summary>
        /// Some useful definitions
        /// </summary>
        int _foundSymbolCount = 0;
        Rect _labelPos = new Rect();

        /// <summary>
        /// Initializes the tracker and other useful structures.
        /// </summary>
        void Start()
        {
            // 1 - Create input instance
            _input = new Texture2DInput(inputImage);

            // 2 - Create results container.
            _result = new FeaturesSymbolFrame();

            // 3 - Create calibration and options
            _calibration = new DeviceCalibration(_input.width, _input.height);
            _options = new TrackerOptions(3, 100);

            // 4 - Define at least one data model.
            _dataModel = new FeaturesModel();
            _dataModel.Read(trainingData);

            // 5 - Create the tracker with defined input, calibration, options and model.
            _tracker = new FeaturesTracker(_calibration, _options);
            _tracker.AppendTrainingData(_dataModel);
            _tracker.input = _input;

             // 6 - Starts tracking. 
            _tracker.Start();
        }

        /// <summary>
        /// Request the results found on the latest frame.
        /// </summary>
        void Update()
        {
            // 7 - Request tracker results. Returns true if some symbol is found.
            if (_tracker.QueryFrame(ref _result))
            {
                _foundSymbolCount = _result.symbolCount;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        void OnDestroy()
        {
            if (_tracker)
                _tracker.Stop();
        }

        /// <summary>
        /// Prints help and tracker results.
        /// </summary>
        void OnGUI()
        {
            // 8 - Show results on screen.
            int screenW = Screen.width;
            int screenH = Screen.height;

            _labelPos.Set(20, 20, screenW, screenH);
            GUI.Label(_labelPos, "This demo is only to understand the basics of FeaturesTracker configuration.");
           
            _labelPos.Set(screenW / 2 - 100, screenH / 2 - 100, 200, 200);
            GUI.DrawTexture(_labelPos, inputImage);

            _labelPos.Set(screenW / 2 - 110, screenH / 2 + 105, screenW, screenH);
            GUI.Label(_labelPos, "FeaturesTracker has found " + _foundSymbolCount + " symbols.");
        }
    }
}