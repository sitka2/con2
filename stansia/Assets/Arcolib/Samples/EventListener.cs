﻿using UnityEngine;

namespace Vexpot.Arcolib.Samples
{
    /// <summary>
    /// 
    /// </summary>
    public class EventListener : MonoBehaviour
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void OnSymbolFound(uint id)
        {
            Debug.Log("Found symbol with id: " + id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void OnSymbolUpdated(uint id)
        {
            Debug.Log("Updated symbol with id:" + id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void OnSymbolLost(uint id)
        {
            Debug.Log("Lost symbol with id:" + id);
        }
    }
}