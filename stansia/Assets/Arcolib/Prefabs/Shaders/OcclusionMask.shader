﻿

// This shader can be used to convert any object into an occluder layer. 

Shader "FX/OcclusionMask"
{
	SubShader
	{
		Tags
	   {
		"Queue" = "Geometry-1"
		"IgnoreProjector" = "True"
		"RenderType" = "TransparentCutout"
	   }

		Lighting Off

		Pass
		{
		ZWrite On
		ZTest LEqual
		ColorMask 0		
		}
	}
}