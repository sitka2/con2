﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Vexpot.Arcolib
{
    /// <summary>
    /// Contains shareable tracker parameters used during detection and tracking.
    /// </summary>
    public class TrackerOptions
    {
        /// <summary>
        /// Creates a new <see cref="TrackerOptions"/> object.
        /// </summary>
        /// <param name="maxSymbolsPerFrame">The amount of symbols allowed to be detected per frame. 
        /// Should be a value between  1 and 10.</param>
        /// <param name="motionTolerance">The motion threshold allowed before skip the detection.</param>
        public TrackerOptions(int maxSymbolsPerFrame, int motionTolerance = 200)
        {
            _nativeTrackerOptionsPtr = TrackerOptionsCreateReference(maxSymbolsPerFrame, motionTolerance);
        }

        /// <summary>
        /// Used to dispose all unmanaged memory.
        /// </summary>
        ~TrackerOptions()
        {
            try
            {
                TrackerOptionsDisposeReference(_nativeTrackerOptionsPtr);
            }
            catch
            {
                throw new Exception("TrackerOptions destructor has failed.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static TrackerOptions Default
        {
            get
            {
                return new TrackerOptions(1, 300);
            }
        }

        /// <summary>
        /// The amount of symbols allowed to be detected per frame.
        /// </summary>
        public int maxSymbolsPerFrame
        {
            get { return TrackerOptionsGetMaxSymbolsPerFrame(_nativeTrackerOptionsPtr); }

            set { TrackerOptionsSetMaxSymbolsPerFrame(_nativeTrackerOptionsPtr, value); }
        }

        /// <summary>
        /// The motion threshold allowed before skip the detection.
        /// </summary>
        public int motionTolerance
        {
            get { return TrackerOptionsGetMotionTolerance(_nativeTrackerOptionsPtr); }

            set { TrackerOptionsSetMotionTolerance(_nativeTrackerOptionsPtr, value); }
        }

        /// <summary>
        /// Pointer to the native object.
        /// </summary>
        internal IntPtr _nativeTrackerOptionsPtr = IntPtr.Zero;

        [DllImport(Config.libraryPath, EntryPoint = "TrackerOptionsCreateReference", CallingConvention = Config.callConvention)]
        internal static extern IntPtr TrackerOptionsCreateReference(int maxSymbolsPerFrame, int motionTolerance);

        [DllImport(Config.libraryPath, EntryPoint = "TrackerOptionsDisposeReference", CallingConvention = Config.callConvention)]
        internal static extern void TrackerOptionsDisposeReference(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "TrackerOptionsSetMaxSymbolsPerFrame", CallingConvention = Config.callConvention)]
        internal static extern void TrackerOptionsSetMaxSymbolsPerFrame(IntPtr ptr, int maxSymbolsPerFrame);

        [DllImport(Config.libraryPath, EntryPoint = "TrackerOptionsGetMaxSymbolsPerFrame", CallingConvention = Config.callConvention)]
        internal static extern int TrackerOptionsGetMaxSymbolsPerFrame(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "TrackerOptionsSetMotionTolerance", CallingConvention = Config.callConvention)]
        internal static extern void TrackerOptionsSetMotionTolerance(IntPtr ptr, int motionTolerance);

        [DllImport(Config.libraryPath, EntryPoint = "TrackerOptionsGetMotionTolerance", CallingConvention = Config.callConvention)]
        internal static extern int TrackerOptionsGetMotionTolerance(IntPtr ptr);
    }

    /// <summary>
    /// A utility class with useful methods used by trackers and other classes.
    /// </summary>
    public static class TrackerUtils{

        /// <summary>
        /// Fast computing for the hash code from string. 
        /// </summary>
        /// <param name="data"> The string data.</param>
        /// <returns></returns>
        static public uint GetHashCode(string data)
        {
            return TrackerUtilsGetHashCode(data);
        }

        [DllImport(Config.libraryPath, EntryPoint = "TrackerUtilsGetHashCode", CallingConvention = Config.callConvention)]
        private static extern uint TrackerUtilsGetHashCode(string data);
    }

    /// <summary>
    ///  Base class for all tracker in the library.
    /// </summary>
    public abstract class Tracker 
    {
        /// <summary>
        /// 
        /// </summary>
        public Tracker()
        {
            _nativeTrackerPtr = IntPtr.Zero;
        }

        /// <summary>
        /// The <see cref="InputSource"/> attached to this tracker.
        /// </summary>
        public abstract InputSource input
        {
           get; set;
        }

        /// <summary>
        /// Starts the analysis on this tracker.
        /// </summary>
        /// <returns></returns>
        public abstract bool Start();

        /// <summary>
        /// Stops the analysis on this tracker.
        /// </summary>
        /// <returns></returns>
        public abstract bool Stop();

        /// <summary>
        /// Sets all tracker settings to its defaults
        /// </summary>
        public abstract void Reset();

        /// <summary>
        /// Checks whether the tracker has started.
        /// </summary>
        public abstract bool isRunning
        {
            get;
        }
        
        /// <summary>
        /// Implicit boolean cast operator to check whether the object exist.
        /// </summary>
        /// <param name="tracker">The object to check.</param>
        public static implicit operator bool (Tracker tracker)
        {
            return (tracker != null);
        }

        internal IntPtr _nativeTrackerPtr;
        /// <summary>
        /// Internal reference to the <see cref="InputSource"/> object.
        /// </summary>
        protected InputSource _inputSource;
        /// <summary>
        ///  Internal reference to the <see cref="DeviceCalibration"/> object.
        /// </summary>
        protected DeviceCalibration _calibration;
        /// <summary>
        /// Internal reference to the <see cref="TrackerOptions"/> object.
        /// </summary>
        protected TrackerOptions _options;
    }

    /// <summary>
    /// The barcode decoding algorithms supported by <see cref="BarcodeTracker"/> class.
    /// </summary>
    [Flags]
    public enum BarcodeType 
    {
        /// <summary>
        /// ArUco markers based dictionary.
        /// </summary>
        ArUco = 0x02,
        /// <summary>
        /// Qr Code dictionary.
        /// </summary>
        QrCode = 0x04,
        /// <summary>
        /// Enables <see cref="ArUco"/> and <see cref="QrCode"/>.
        /// </summary>
        ArUcoAndQrCode = ArUco | QrCode
    };

    /// <summary>
    /// Tracker used to track faces in the scene.
    /// </summary>
    public class FaceTracker : Tracker
    {
        /// <summary>
        /// Creates a new <see cref="FaceTracker"/> instance.
        /// </summary>
        /// <param name="calibration"></param>
        /// <param name="options"></param>
        public FaceTracker(DeviceCalibration calibration, TrackerOptions options) : base(){

            if (calibration != null && options != null)
            {
                _nativeTrackerPtr = FaceTrackerCreateReference(calibration._nativeDeviceCalibrationPtr, options._nativeTrackerOptionsPtr);
                _calibration = calibration;
                _options = options;
            }
        }

        /// <summary>
        /// Used to dispose all native memory.
        /// </summary>
        ~FaceTracker()
        {
            Stop();           
            FaceTrackerDisposeReference(_nativeTrackerPtr);
        }

        /// <summary>
        /// The <see cref="InputSource"/> attached to this tracker.
        /// </summary>
        public override InputSource input
        {
            get { return _inputSource; }
            set
            {
                if (value != null)
                {
                   FaceTrackerSetInput(_nativeTrackerPtr, value._nativeInputSourcePtr);
                   _inputSource = value;
                }
            }
        }

        /// <summary>
        /// Starts the face detection or tracking.
        /// </summary>
        /// <returns>True if the tracker start succeeded.</returns>
        public override bool Start()
        {
            return FaceTrackerStart(_nativeTrackerPtr);
        }

        /// <summary>
        /// Stops the face detection or tracking.
        /// </summary>
        /// <returns>True if the tracker stop succeeded.</returns>
        public override bool Stop()
        {
            return FaceTrackerStop(_nativeTrackerPtr);
        }

        /// <summary>
        /// Sets all tracker settings to its defaults
        /// </summary>
        public override void Reset()
        {
            FaceTrackerReset(_nativeTrackerPtr);
        }

        /// <summary>
        /// Checks whether the tracker has started.
        /// </summary>
        public override bool isRunning
        {
            get { return FaceTrackerIsRunning(_nativeTrackerPtr); }
        }

        /// <summary>
        /// Register the <see cref="FaceModel"/> to be computed by this tracker.
        /// </summary>
        /// <param name="model">The model with faces training data.</param>
        public void SetTrainingData(FaceModel model)
        {
            try {
                FaceTrackerSetTrainingData(_nativeTrackerPtr, model._nativeDataModelPtr);
            }
            catch
            {
                throw new ArgumentException("Native FaceTracker.setTrainingData unhandled exception.");
            }
        }

        /// <summary>
        /// Requests the latest frame computed by this tracker.
        /// </summary>
        /// <remarks>
        /// All data contained in the <see cref="FaceSymbolFrame"/> are replaced 
        /// with the new results. If you need to keep a copy of the previous frame use 
        /// <see cref="FaceSymbolFrame.Clone"/>.
        /// </remarks>
        /// <param name="frame">The frame to store the new results.</param>
        /// <returns>True if a new frame is grabbed.</returns>
        public bool QueryFrame(ref FaceSymbolFrame frame)
        {
            return FaceTrackerQueryFrame(_nativeTrackerPtr, ref frame);
        }

        [DllImport(Config.libraryPath, EntryPoint = "FaceTrackerCreateReference", CallingConvention = Config.callConvention)]
        private static extern IntPtr FaceTrackerCreateReference(IntPtr calibration, IntPtr options);

        [DllImport(Config.libraryPath, EntryPoint = "FaceTrackerSetInput", CallingConvention = Config.callConvention)]
        private static extern void FaceTrackerSetInput(IntPtr ptr, IntPtr input);

        [DllImport(Config.libraryPath, EntryPoint = "FaceTrackerStart", CallingConvention = Config.callConvention)]
        private static extern bool FaceTrackerStart(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FaceTrackerStop", CallingConvention = Config.callConvention)]
        private static extern bool FaceTrackerStop(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FaceTrackerReset", CallingConvention = Config.callConvention)]
        private static extern void FaceTrackerReset(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FaceTrackerIsRunning", CallingConvention = Config.callConvention)]
        private static extern bool FaceTrackerIsRunning(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FaceTrackerQueryFrame", CallingConvention = Config.callConvention)]
        private static extern bool FaceTrackerQueryFrame(IntPtr ptr, ref FaceSymbolFrame frame);

        [DllImport(Config.libraryPath, EntryPoint = "FaceTrackerSetTrainingData", CallingConvention = Config.callConvention)]
        private static extern void FaceTrackerSetTrainingData(IntPtr ptr, IntPtr model);

        [DllImport(Config.libraryPath, EntryPoint = "FaceTrackerDisposeReference", CallingConvention = Config.callConvention)]
        private static extern void FaceTrackerDisposeReference(IntPtr ptr);
    }

    /// <summary>
    /// Tracker used to detect or track barcodes of certain types.
    /// </summary>
    public class BarcodeTracker : Tracker
    {
        /// <summary>
        /// Creates a new <see cref="BarcodeTracker"/> instance.
        /// </summary>
        /// <param name="calibration"></param>
        /// <param name="options"></param>
        public BarcodeTracker(DeviceCalibration calibration, TrackerOptions options) : base()
        {
            if (calibration != null && options != null)
            {
                _nativeTrackerPtr = BarcodeTrackerCreateReference(calibration._nativeDeviceCalibrationPtr, options._nativeTrackerOptionsPtr);
                _calibration = calibration;
                _options = options; 
            }
        }

        /// <summary>
        /// Used to dispose all native memory.
        /// </summary>
        ~BarcodeTracker()
        {
            Stop();
            BarcodeTrackerDisposeReference(_nativeTrackerPtr);
        }

        /// <summary>
        /// The <see cref="InputSource"/> attached to this tracker.
        /// </summary>
        public override InputSource input
        {
            get { return _inputSource; }
            set
            {
                if (value != null)
                {
                    BarcodeTrackerSetInput(_nativeTrackerPtr, value._nativeInputSourcePtr);
                    _inputSource = value;
                }
            }
        }

        /// <summary>
        /// Starts the barcode detection and tracking.
        /// </summary>
        /// <returns>True if the tracker start succeeded.</returns>
        public override bool Start()
        {
            return BarcodeTrackerStart(_nativeTrackerPtr);
        }

        /// <summary>
        /// Stops the barcode detection and tracking.
        /// </summary>
        /// <returns>True if the tracker stop succeeded.</returns>
        public override bool Stop()
        {
            return BarcodeTrackerStop(_nativeTrackerPtr);
        }

        /// <summary>
        /// Sets all tracker settings to its defaults
        /// </summary>
        public override void Reset()
        {
            BarcodeTrackerReset(_nativeTrackerPtr);
        }

        /// <summary>
        /// Checks whether the tracker has started.
        /// </summary>
        public override bool isRunning
        {
           get { return BarcodeTrackerIsRunning(_nativeTrackerPtr); }
        }

        /// <summary>
        /// Defines the <see cref="BarcodeType"/> algorithms to use during detection phase.
        /// </summary>
        /// <remarks>
        /// You can combine multiple <see cref="BarcodeType"/> as flags. 
        /// </remarks>
        public BarcodeType useBarcodeType
        {
            get { return _currentBarcodeType; }

            set { _currentBarcodeType = value;
                BarcodeTrackerSetBarcodeType(_nativeTrackerPtr, (int)_currentBarcodeType);
            }
        }

        /// <summary>
        /// Requests and obtains the latest frame computed by this tracker.
        /// </summary>
        /// <remarks>
        /// All data contained in the <see cref="BarcodeSymbolFrame"/> are replaced 
        /// by the new results. If you need to keep a copy of the previous frame use 
        /// <see cref="BarcodeSymbolFrame.Clone"/>.
        /// </remarks>
        /// <param name="frame">The frame to store the new results.</param>
        /// <returns>True if a new frame is grabbed.</returns>
        public bool QueryFrame(ref BarcodeSymbolFrame frame)
        {
           return BarcodeTrackerQueryFrame(_nativeTrackerPtr, ref frame);
        }

        /// <summary>
        /// 
        /// </summary>
        private BarcodeType _currentBarcodeType = BarcodeType.ArUco;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="calibration"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        [DllImport(Config.libraryPath, EntryPoint = "BarcodeTrackerCreateReference", CallingConvention = Config.callConvention)]
        private static extern IntPtr BarcodeTrackerCreateReference(IntPtr calibration, IntPtr options);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeTrackerSetInput", CallingConvention = Config.callConvention)]
        private static extern void BarcodeTrackerSetInput(IntPtr ptr, IntPtr input);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeTrackerStart", CallingConvention = Config.callConvention)]
        private static extern bool BarcodeTrackerStart(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeTrackerStop", CallingConvention = Config.callConvention)]
        private static extern bool BarcodeTrackerStop(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeTrackerSetBarcodeType", CallingConvention = Config.callConvention)]
        private static extern void BarcodeTrackerSetBarcodeType(IntPtr ptr, int value);        

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeTrackerReset", CallingConvention = Config.callConvention)]
        private static extern void BarcodeTrackerReset(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeTrackerIsRunning", CallingConvention = Config.callConvention)]
        private static extern bool BarcodeTrackerIsRunning(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeTrackerQueryFrame", CallingConvention = Config.callConvention)]
        private static extern bool BarcodeTrackerQueryFrame(IntPtr ptr, ref BarcodeSymbolFrame frame);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeTrackerDisposeReference", CallingConvention = Config.callConvention)]
        private static extern void BarcodeTrackerDisposeReference(IntPtr ptr);
    }

    /// <summary>
    ///  Tracker used to detect or track natural images.
    /// </summary>
    public class FeaturesTracker : Tracker
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="calibration"></param>
        /// <param name="options"></param>
        public FeaturesTracker(DeviceCalibration calibration, TrackerOptions options) : base()
        {
            if (calibration != null && options != null)
            {
                _nativeTrackerPtr = FeaturesTrackerCreateReference(calibration._nativeDeviceCalibrationPtr, options._nativeTrackerOptionsPtr);
                _calibration = calibration;
                _options = options;            
            }

            _modelList = new List<FeaturesModel>();
        }

        /// <summary>
        /// Used to dispose all native memory.
        /// </summary>
        ~FeaturesTracker()
        {
            Stop();
            FeaturesTrackerDisposeReference(_nativeTrackerPtr);       
        }

        /// <summary>
        /// The <see cref="InputSource"/> attached to this tracker.
        /// </summary>
        public override InputSource input
        {
            get { return _inputSource; }
            set
            {
                if (value != null)
                {
                    FeaturesTrackerSetInput(_nativeTrackerPtr, value._nativeInputSourcePtr);
                    _inputSource = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public FeaturesModel GetTrainingDataByIndex(int index)
        {
            if (index >= 0 && index < _modelList.Count)
            {
                return _modelList[index];
            }
            else return null;
        }

        /// <summary>
        /// Starts the features detection and tracking.
        /// </summary>
        /// <returns>True if the tracker start succeeded.</returns>
        public override bool Start()
        {
            return FeaturesTrackerStart(_nativeTrackerPtr);
        }

        /// <summary>
        /// Stops the features detection and tracking.
        /// </summary>
        /// <returns>True if the tracker stop succeeded.</returns>
        public override bool Stop()
        {
            return FeaturesTrackerStop(_nativeTrackerPtr);
        }

        /// <summary>
        /// Sets all tracker settings to its defaults.
        /// </summary>
        public override void Reset()
        {
            _modelList.Clear();
            FeaturesTrackerReset(_nativeTrackerPtr);
        }

        /// <summary>
        /// Checks whether the tracker has started.
        /// </summary>
        public override bool isRunning
        {
            get { return FeaturesTrackerIsRunning(_nativeTrackerPtr); }
        }

        /// <summary>
        /// Register a set of models to be computed by this tracker.
        /// </summary>
        /// <param name="modelList">The list of models to register.</param>
        /// <param name="keepPreviousModels">If you need to keep previous models set this to true.</param>
        public void SetTrainingData(List<FeaturesModel> modelList, bool keepPreviousModels = false)
        {
            if (!keepPreviousModels)
                Reset();
            foreach (var m in modelList)
                AppendTrainingData(m);
        }

        /// <summary>
        /// Adds a new <see cref="FeaturesModel"/> to be computed by this tracker.
        /// </summary>
        /// <param name="model">The model to be added.</param>
        public void AppendTrainingData(FeaturesModel model)
        {
            if (model.qualityLevel == 0) {
                throw new ArgumentException("You must initialize the model with valid data before used!");
            }

            try {
                _modelList.Add(model);
                FeaturesTrackerAppendTrainingData(_nativeTrackerPtr, model._nativeDataModelPtr);
            }
            catch 
            {
                throw new ArgumentException("Native appendTrainingData non handled exception.");
            }
        }

        /// <summary>
        /// Set this to true if you don't need tracking.
        /// </summary>
        /// <remarks>
        /// Default value is false. 
        /// </remarks>
        public bool detectOnly
        {
            get { return FeaturesTrackerDetectOnly(_nativeTrackerPtr); }
            set { FeaturesTrackerSetDetectOnly(_nativeTrackerPtr, value); }
        }

        /// <summary>
        /// Defines the minimum amount of matches needed to 
        /// consider a detection as valid.
        /// </summary>
        /// <remarks>
        /// The minimum allowed value is 20. Beware that if you increase the value 
        /// too much the tracker might not detect anything.
        /// Default value is 30.
        /// </remarks>
        public int minMatchCount
        {
            get { return FeaturesTrackerMinMatchCount(_nativeTrackerPtr); }
            set { FeaturesTrackerSetMinMatchCount(_nativeTrackerPtr, value); }
        }

        /// <summary>
        /// Use this property to check whether the input source used by this tracker 
        /// is providing a good image to be processed.
        /// </summary>
        /// <remarks>
        /// Internally the tracker will skip all frames that contains a poor quality 
        /// but you can use this property to create some notification on user interface.
        /// (i.e the place is too dark or the image is not descriptive enough).
        /// </remarks>
        public bool hasEnoughFeatures
        {
            get { return FeaturesTrackerInputHasEnoughFeatures(_nativeTrackerPtr); }
        }

        /// <summary>
        /// Requests and obtains the latest frame computed by this tracker.
        /// </summary>
        /// <remarks>
        /// All data contained in the <see cref="FeaturesSymbolFrame"/> are replaced 
        /// by the new results. If you need to keep a copy of the previous frame use 
        /// <see cref="FeaturesSymbolFrame.Clone"/>.
        /// </remarks>
        /// <param name="frame">The frame to store the new results.</param>
        /// <returns>True if a new frame is grabbed.</returns>
        public bool QueryFrame(ref FeaturesSymbolFrame frame)
        {
           return FeaturesTrackerQueryFrame(_nativeTrackerPtr, ref frame);
        }

        private List<FeaturesModel> _modelList;

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerCreateReference", CallingConvention = Config.callConvention)]
        private static extern IntPtr FeaturesTrackerCreateReference(IntPtr calibration, IntPtr options);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerSetInput", CallingConvention = Config.callConvention)]
        private static extern void FeaturesTrackerSetInput(IntPtr ptr, IntPtr input);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerStart", CallingConvention = Config.callConvention)]
        private static extern bool FeaturesTrackerStart(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerStop", CallingConvention = Config.callConvention)]
        private static extern bool FeaturesTrackerStop(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerReset", CallingConvention = Config.callConvention)]
        private static extern void FeaturesTrackerReset(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerIsRunning", CallingConvention = Config.callConvention)]
        private static extern bool FeaturesTrackerIsRunning(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerQueryFrame", CallingConvention = Config.callConvention)]
        private static extern bool FeaturesTrackerQueryFrame(IntPtr ptr, ref FeaturesSymbolFrame frame);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerAppendTrainingData", CallingConvention = Config.callConvention)]
        private static extern void FeaturesTrackerAppendTrainingData(IntPtr ptr, IntPtr model);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerDetectOnly", CallingConvention = Config.callConvention)]
        private static extern bool FeaturesTrackerDetectOnly(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerSetDetectOnly", CallingConvention = Config.callConvention)]
        private static extern void FeaturesTrackerSetDetectOnly(IntPtr ptr, bool value);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerMinMatchCount", CallingConvention = Config.callConvention)]
        private static extern int FeaturesTrackerMinMatchCount(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerSetMinMatchCount", CallingConvention = Config.callConvention)]
        private static extern void FeaturesTrackerSetMinMatchCount(IntPtr ptr, int value);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerInputHasEnoughFeatures", CallingConvention = Config.callConvention)]
        private static extern bool FeaturesTrackerInputHasEnoughFeatures(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesTrackerDisposeReference", CallingConvention = Config.callConvention)]
        private static extern void FeaturesTrackerDisposeReference(IntPtr ptr);
    }
}
