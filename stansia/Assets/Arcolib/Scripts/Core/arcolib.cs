﻿

namespace Vexpot.Arcolib
{
    /// <summary>
    ///  Provides some important info about the library.
    /// </summary>
    static public class arcolib
    {
        /// <summary>
        /// Holds the current library version. 
        /// </summary>
        public const string version = "1.0.3b";
        /// <summary>
        /// Copyright info.
        /// </summary>
        public const string copyright = "Copyright © 2017 Vexpot. All rights reserved.";
        /// <summary>
        /// The library's author.
        /// </summary>
        public const string author = "Yoan Escobar";
    }
}
