﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Vexpot.Arcolib
{
    /// <summary>
    /// Defines an abstract device calibration used to convert projective coordinates to
    /// Unity world coordinates using the proper <see cref="Camera.projectionMatrix"/> 
    /// </summary>
    /// <remarks>
    /// For advanced scenarios you may need multiple calibration over the same <see cref="InputSource"/>.
    /// </remarks>
    public class DeviceCalibration
    {
        /// <summary>
        /// Creates a new <see cref="DeviceCalibration"/> instance.
        /// </summary>
        /// <param name="width">Source image with in pixels.</param>
        /// <param name="height">Source image height in pixels.</param>
        /// <param name="apertureWidth">Physical width in mm of the sensor.</param>
        /// <param name="apertureHeight">Physical height in mm of the sensor.</param>
        public DeviceCalibration(int width, int height, float apertureWidth = 0, float apertureHeight = 0)
        {
            _nativeDeviceCalibrationPtr = DeviceCalibrationCreateReference(width, height, apertureWidth, apertureHeight);
        }

        /// <summary>
        /// Used to dispose all unmanaged memory allocated.
        /// </summary>
        ~DeviceCalibration()
        {
            DeviceCalibrationDisposeReference(_nativeDeviceCalibrationPtr);
        }

        /// <summary>
        ///  Request a new calibration.
        /// </summary>
        /// <param name="width">Source image with in pixels.</param>
        /// <param name="height">Source image height in pixels.</param>
        /// <param name="apertureWidth">Physical width in mm of the sensor.</param>
        /// <param name="apertureHeight">Physical height in mm of the sensor.</param>
        public void Calibrate(int width, int height, float apertureWidth, float apertureHeight)
        {
            DeviceCalibrationCalibrate(_nativeDeviceCalibrationPtr, width, height, apertureWidth, apertureHeight);
        }

        /// <summary>
        /// Gets the horizontal field of view in the current calibration.
        /// </summary>
        public float horizontalFieldOfView {
            get { return DeviceCalibrationGetHorizontalFieldOfView(_nativeDeviceCalibrationPtr); }
        }

        /// <summary>
        /// Gets the vertical field of view in the current calibration. 
        /// </summary>
        public float verticalFieldOfView
        {
            get { return DeviceCalibrationGetVerticalFieldOfView(_nativeDeviceCalibrationPtr); }
        }

        /// <summary>
        /// The estimated focal distance.
        /// </summary>
        public float focalLength
        {
            get { return DeviceCalibrationGetFocalLength(_nativeDeviceCalibrationPtr); }
        }

        /// <summary>
        /// Proportion between source width and height.
        /// </summary>
        public float aspectRatio
        {
            get { return DeviceCalibrationGetAspectRatio(_nativeDeviceCalibrationPtr); }
        }

        internal IntPtr _nativeDeviceCalibrationPtr = IntPtr.Zero;

        [DllImport(Config.libraryPath, EntryPoint = "DeviceCalibrationCreateReference", CallingConvention = Config.callConvention)]
        private static extern IntPtr DeviceCalibrationCreateReference(int width, int height, float apertureWidth, float apertureHeight);

        [DllImport(Config.libraryPath, EntryPoint = "DeviceCalibrationDisposeReference", CallingConvention = Config.callConvention)]
        private static extern void DeviceCalibrationDisposeReference(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "DeviceCalibrationCalibrate", CallingConvention = Config.callConvention)]
        private static extern void DeviceCalibrationCalibrate(IntPtr ptr, int width, int height, float apertureWidth, float apertureHeight);

        [DllImport(Config.libraryPath, EntryPoint = "DeviceCalibrationGetHorizontalFieldOfView", CallingConvention = Config.callConvention)]
        private static extern float DeviceCalibrationGetHorizontalFieldOfView(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "DeviceCalibrationGetVerticalFieldOfView", CallingConvention = Config.callConvention)]
        private static extern float DeviceCalibrationGetVerticalFieldOfView(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "DeviceCalibrationGetFocalLength", CallingConvention = Config.callConvention)]
        private static extern float DeviceCalibrationGetFocalLength(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "DeviceCalibrationGetAspectRatio", CallingConvention = Config.callConvention)]
        private static extern float DeviceCalibrationGetAspectRatio(IntPtr ptr);
    }
}
