﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Vexpot.Arcolib
{
    /// <summary>
    /// Defines the direction during image flip operations.
    /// </summary>
    public enum ImageFlipMode
    {
        /// <summary>
        /// The flip is disabled.
        /// </summary>
        None,
        /// <summary>
        /// Flips the image horizontally.
        /// </summary>
        Horizontal,
        /// <summary>
        /// Flips the image vertically.
        /// </summary>
        Vertical,
        /// <summary>
        /// Flips the image both horizontally and vertically.
        /// </summary>
        Both
    }

    /// <summary>
    /// This class contains a set of tools used to handle images.
    /// Some functions are integrated with Unity objects in order to facilitate it use.
    /// </summary>
    static public class ImageUtils
    {
        /// <summary>
        /// Loads a local image stored in your file system. 
        /// </summary>
        /// <remarks>
        /// The resulting <see cref="Texture2D"/> is always in <see cref="TextureFormat.RGB24"/> format.
        /// When loading images with alpha channel (like PNG) this channel is omitted.
        /// If you need alpha channel consider using <see cref="Texture2D.LoadImage(byte[])"/> instead.
        /// </remarks>
        /// <param name="imageFile"></param>
        /// <param name="flipMode"></param>
        /// <returns>The image representation in a <see cref="Texture2D"/> object.</returns>
        static public Texture2D LoadImage(string imageFile, ImageFlipMode flipMode = ImageFlipMode.None)
        {
            Texture2D texture = null;
            int w = 0, h = 0, bpp = 0;
            if (File.Exists(imageFile))
            {
                try
                {
                    IntPtr data = LoadImageInBuffer(imageFile, ref w, ref h, ref bpp, (int)flipMode);
                    texture = new Texture2D(w, h, (TextureFormat)bpp, false);
                    texture.LoadRawTextureData(data, w * h * bpp);
                    texture.mipMapBias = -0.3f;
                    texture.Apply(false);
                    DisposeImageBuffer(data);
                }
                catch
                {
                    throw new InvalidOperationException("LoadImage has failed!");
                }
            }
            return texture;
        }

        /// <summary>
        /// Converts <see cref="WebCamTexture"/> source to data buffer and performs flips transformation.
        /// </summary>
        /// <param name="wcamTexture"></param>
        /// <param name="data"></param>
        /// <param name="mode"></param>
        /// <param name="colors"></param>
        static public void WebCamTextureToData(WebCamTexture wcamTexture, byte[] data, ImageFlipMode mode, Color32[] colors = null)
        {
            if (wcamTexture != null || data != null)
            {
                GCHandle pixelsHandle = default(GCHandle);
                try
                {
                    if (colors == null)
                    {
                        Color32[] pixels32 = wcamTexture.GetPixels32();
                        pixelsHandle = GCHandle.Alloc(pixels32, GCHandleType.Pinned);
                    }
                    else {
                        wcamTexture.GetPixels32(colors);
                        pixelsHandle = GCHandle.Alloc(colors, GCHandleType.Pinned);
                    }

                    int wcamWidth = wcamTexture.width;
                    int wcamHeight = wcamTexture.height;
                    FlipImagePixels(pixelsHandle.AddrOfPinnedObject(), wcamWidth, wcamHeight, 4, mode);
                    Marshal.Copy(pixelsHandle.AddrOfPinnedObject(), data, 0, wcamWidth * wcamHeight * 4);
                }

                catch
                {
                    throw new InvalidOperationException("WebCamTextureToData has failed!");
                }

                finally
                {
                    if (pixelsHandle != default(GCHandle))
                        pixelsHandle.Free();
                }
            }
            else throw new ArgumentNullException("Arguments cant not be null!");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pixels"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="bytesPerPixel"></param>
        /// <param name="mode"></param>
        static public void FlipImagePixels(byte[] pixels, int width, int height, int bytesPerPixel, ImageFlipMode mode)
        {
            try
            {
                if (pixels != null)
                {
                    switch (mode)
                    {
                        case ImageFlipMode.Vertical: FlipPixelsBytesV(pixels, width, height, bytesPerPixel); break;
                        case ImageFlipMode.Horizontal: FlipPixelsBytesH(pixels, width, height, bytesPerPixel); break;
                        case ImageFlipMode.Both: FlipPixelsBytesVH(pixels, width, height, bytesPerPixel); break;
                    }
                }
            }
            catch
            {
                throw new InvalidOperationException("FlipImagePixels has failed!");
            }
        }

        /// <summary>
        ///  Flips image buffer using the <see cref="ImageFlipMode"/>.
        /// </summary>
        /// <param name="pixels"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="bytesPerPixel"></param>
        /// <param name="mode"></param>
        static public void FlipImagePixels(IntPtr pixels, int width, int height, int bytesPerPixel, ImageFlipMode mode)
        {
            try
            {
                if (pixels != IntPtr.Zero)
                {
                    switch (mode)
                    {
                        case ImageFlipMode.Vertical: FlipImageV(pixels, width, height, bytesPerPixel); break;
                        case ImageFlipMode.Horizontal: FlipImageH(pixels, width, height, bytesPerPixel); break;
                        case ImageFlipMode.Both: FlipImageVandH(pixels, width, height, bytesPerPixel); break;
                    }
                }
            }
            catch
            {
                throw new InvalidOperationException("FlipImageBuffer has failed!");
            }
        }


        /// <summary>
        /// Applies a pixelation filter to the image buffer.
        /// This function is only for developing purposes. 
        /// Consider using a shader implementation instead.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="bytesPerPixel"></param>
        /// <param name="pixelationLevel"></param>
        static public void PixelateImage(IntPtr data, int width, int height, int bytesPerPixel, int pixelationLevel)
        {
            try
            {
                PixelateImageInternal(data, width, height, bytesPerPixel, pixelationLevel);
            }
            catch
            {
                throw new InvalidOperationException("PixelateImage has failed!");
            }
        }

        /// <summary>
        /// Return the focus level on the image. 
        /// This function is computationally expensive, use wisely.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="bytesPerPixel"></param>
        /// <returns></returns>
        static public float DetectFocusLevel(IntPtr data, int width, int height, int bytesPerPixel)
        {
            float level = 0.0f;
            try
            {
                level = DetectFocusLevelInternal(data, width, height, bytesPerPixel);
            }
            catch
            {
                throw new InvalidOperationException("DetectFocusLevel has failed!");
            }
            return level;
        }


        [DllImport(Config.libraryPath, EntryPoint = "LoadImageInBuffer", CallingConvention = Config.callConvention)]
        private static extern IntPtr LoadImageInBuffer(string filename, ref int width, ref int height, ref int bytesPerPixel, int flipMode);

        [DllImport(Config.libraryPath, EntryPoint = "DisposeImageBuffer", CallingConvention = Config.callConvention)]
        private static extern void DisposeImageBuffer(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FlipImageV", CallingConvention = Config.callConvention)]
        private static extern void FlipImageV(IntPtr ptr, int width, int height, int bytesPerPixel);

        [DllImport(Config.libraryPath, EntryPoint = "FlipImageV", CallingConvention = Config.callConvention)]
        private static extern void FlipPixelsBytesV(byte[] pixels, int width, int height, int bytesPerPixel);

        [DllImport(Config.libraryPath, EntryPoint = "FlipImageH", CallingConvention = Config.callConvention)]
        private static extern void FlipImageH(IntPtr ptr, int width, int height, int bytesPerPixel);

        [DllImport(Config.libraryPath, EntryPoint = "FlipImageH", CallingConvention = Config.callConvention)]
        private static extern void FlipPixelsBytesH(byte [] pixels, int width, int height, int bytesPerPixel);

        [DllImport(Config.libraryPath, EntryPoint = "FlipImageVandH", CallingConvention = Config.callConvention)]
        private static extern void FlipImageVandH(IntPtr ptr, int width, int height, int bytesPerPixel);

        [DllImport(Config.libraryPath, EntryPoint = "FlipImageVandH", CallingConvention = Config.callConvention)]
        private static extern void FlipPixelsBytesVH(byte[] pixels, int width, int height, int bytesPerPixel);

        [DllImport(Config.libraryPath, EntryPoint = "PixelateImage", CallingConvention = Config.callConvention)]
        private static extern void PixelateImageInternal(IntPtr data, int width, int height, int bytesPerPixel, int pixelationLevel);

        [DllImport(Config.libraryPath, EntryPoint = "DetectFocusLevel", CallingConvention = Config.callConvention)]
        private static extern float DetectFocusLevelInternal(IntPtr data, int width, int height, int bytesPerPixel);

    }
}
