﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

namespace Vexpot.Arcolib
{
    /// <summary>
    /// Base class for all capture implementation.
    /// </summary>
    public class Capture
    {
        /// <summary>
        /// 
        /// </summary>
        protected Capture()
        {
            _nativePtr = IntPtr.Zero;
            _framePixelsPtr = IntPtr.Zero;
            _allocatedBytesCount = 0;
            _framePixels = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytesCount"></param>
        protected void AllocateFramePixelsWhenNeeded(uint bytesCount)
        {
            if (_framePixelsPtr == IntPtr.Zero || _allocatedBytesCount != bytesCount)
            {
                if (_framePixelsPtr != IntPtr.Zero)
                    Marshal.FreeHGlobal(_framePixelsPtr);

                _allocatedBytesCount = (int)bytesCount;
                _framePixelsPtr = Marshal.AllocHGlobal(_allocatedBytesCount);
                _framePixels = new byte[_allocatedBytesCount];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        internal IntPtr _nativePtr;
        /// <summary>
        /// 
        /// </summary>
        protected IntPtr _framePixelsPtr;
        /// <summary>
        /// 
        /// </summary>
        protected byte[] _framePixels;
        /// <summary>
        /// 
        /// </summary>
        protected int _allocatedBytesCount;
    }

    /// <summary>
    /// The <see cref="CameraCapture"/> class provides an interface for system camera devices.
    /// </summary>
    public class CameraCapture : Capture
    {
        /// <summary>
        /// Creates a new <see cref="CameraCapture"/> instance.
        /// </summary>
        public CameraCapture() : base()
        {

#if UNITY_ANDROID && !UNITY_EDITOR
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
            AndroidJavaClass cameraCapture = new AndroidJavaClass("arcolib.CameraCapture");
            cameraCapture.CallStatic("setContext", context);
            cameraCapture.CallStatic("setCurrentActivity", activity);            
#endif
            _nativePtr = CameraCaptureCreateReference();
        }

        /// <summary>
        ///  Destructor.
        /// </summary>
        ~CameraCapture()
        {
            CameraCaptureDisposeReference(_nativePtr);
            if (_framePixelsPtr != IntPtr.Zero)
                Marshal.FreeHGlobal(_framePixelsPtr);
        }

        /// <summary>
        /// Opens the capturing device and starts playing.
        /// </summary>
        /// <param name="deviceIndex"> The device index on the system. </param>
        /// <param name="width"> The desired width for captured image.</param>
        /// <param name="height">The desired height for captured image.</param>
        /// <returns></returns>
        public bool Open(uint deviceIndex, uint width, uint height)
        {
            bool success = true;
            try
            {
                success = CameraCaptureOpen(_nativePtr, deviceIndex, width, height);
            }
            catch
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        /// Opens the capturing device and starts playing.
        /// </summary>
        /// <param name="deviceIndex"> The device index on the system. </param>
        /// <param name="format"> A valid capture format supported by this device. See <see cref="FindSupportedFormats"/>. </param>
        /// <returns></returns>
        public bool Open(uint deviceIndex, CaptureFormat format)
        {
            return Open(deviceIndex, format.width, format.height);
        }

   
        /// <summary>
        /// Closes the capturing device.
        /// </summary>
        public void Close()
        {
            try
            {
                CameraCaptureClose(_nativePtr);
            }
            catch
            {
                throw new InvalidOperationException("Cannot close this camera.");
            }
        }

        /// <summary>
        /// Returns true if camera sensor has been opened already.
        /// </summary>
        public bool isOpened
        {
            get
            {
                return CameraCaptureIsOpened(_nativePtr);
            }
        }

        /// <summary>
        ///  Returns the latest captured frame.
        /// </summary>
        /// <param name="pixels"></param>
        /// <param name="bytesCount"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="bpp"></param>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        public bool ReadFrame(out byte[] pixels, out uint bytesCount, out uint width, out uint height, out uint bpp, out ulong timestamp)
        {
            timestamp = 0;
            width = height = bytesCount = bpp = 0;
            pixels = null;

            if (CameraCaptureGrab(_nativePtr, ref width, ref height, ref bpp))
            {
                AllocateFramePixelsWhenNeeded(width * height * bpp);
                CameraCaptureReadFrame(_nativePtr, _framePixelsPtr, ref bytesCount, ref width, ref height, ref bpp, ref timestamp);
                Marshal.Copy(_framePixelsPtr, _framePixels, 0, _allocatedBytesCount);
                pixels = _framePixels;
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Returns the latest captured frame.
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        public bool ReadFrame(ref ImageFrame frame)
        {
            if (CameraCaptureGrab(_nativePtr, ref frame.width, ref frame.height, ref frame.bpp))
            {
                AllocateFramePixelsWhenNeeded(frame.width * frame.height * frame.bpp);
                CameraCaptureReadFrame(_nativePtr, _framePixelsPtr, ref frame.bytesCount, ref frame.width, ref frame.height, ref frame.bpp, ref frame.timestamp);
                Marshal.Copy(_framePixelsPtr, _framePixels, 0, _allocatedBytesCount);
                frame.data = _framePixels;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Reads the image frame with flipped byte to properly render over Texture2D.
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        public bool ReadFrame(Texture2D frame)
        {
            uint width = 0, height = 0, bpp = 0;
            if (CameraCaptureGrab(_nativePtr, ref width, ref height, ref bpp))
            {
                AllocateFramePixelsWhenNeeded(width * height * bpp);
                CameraCaptureReadFrameToTexture2D(_nativePtr, _framePixelsPtr);              

                if (frame.width != width || frame.height != height)
                {
                    frame.Resize((int)width, (int)height);
                }

                frame.LoadRawTextureData(_framePixelsPtr, _allocatedBytesCount);
                frame.Apply(false);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Sets or gets the pixels format.
        /// </summary>
        /// <remarks>
        /// If you need to change the pixels format you must set this property before opening the camera. 
        /// Changing this property while the camera is open will not have any effect.
        /// </remarks>
        public PixelsFormat pixelsFormat
        {
            set
            {
                CameraCaptureSetPixelsFormat(_nativePtr, value);
            }
            get
            {
                return CameraCaptureGetPixelsFormat(_nativePtr);
            }
        }

        /// <summary>
        /// Sets or gets the zoom factor.
        /// </summary>
        public float zoom
        {
            set
            {
                CameraCaptureSetZoom(_nativePtr, value);
            }
            get
            {
                return CameraCaptureGetZoom(_nativePtr);
            }
        }

        /// <summary>
        /// Indicates whether the zoom factor is supported by the camera.
        /// </summary>
        public bool isZoomSupported
        {
            get
            {
                return CameraCaptureIsZoomSupported(_nativePtr);
            }
        }

        /// <summary>
        /// The minimum zoom factor allowed in the current device.
        /// </summary>
        public float minZoom
        {
            get
            {
                return CameraCaptureMinZoom(_nativePtr);
            }
        }

        /// <summary>
        /// The maximum zoom factor allowed in the current device.
        /// </summary>
        public float maxZoom
        {
            get
            {
                return CameraCaptureMaxZoom(_nativePtr);
            }
        }

        /// <summary>
        /// The current torch brightness level.
        /// </summary>
        /// <remarks>
        /// The value of this property is a floating-point number whose value is in the range 0.0 to 1.0. 
        /// A torch level of 0.0 indicates that the torch is off. 
        /// A torch level of 1.0 represents the maximum value.
        /// </remarks>
        public float torchLevel
        {
            set
            {
                CameraCaptureSetTorchLevel(_nativePtr, value);
            }
        }

        /// <summary>
        /// Gets or sets the current torch mode.
        /// </summary>
        public TorchMode torchMode
        {
            set
            {
                CameraCaptureSetTorchMode(_nativePtr, value);
            }
            get
            {
                return CameraCaptureGetTorchMode(_nativePtr);
            }
        }

        /// <summary>
        /// Indicates whether the torch is currently available for use.
        /// </summary>
        public bool isTorchAvailable
        {
            get
            {
                return CameraCaptureIsTorchAvailable(_nativePtr);
            }
        }

        /// <summary>
        /// Indicates whether the device supports torch brightness level adjustments.
        /// </summary>
        public bool isTorchLevelSupported
        {
            get
            {
                return CameraCaptureIsTorchLevelSupported(_nativePtr);
            }
        }


        /// <summary>
        /// Indicates whether the device supports the specified torch mode.
        /// </summary>
        /// <param name="torchMode"></param>
        /// <returns></returns>
        public bool IsTorchModeSupported(TorchMode torchMode)
        {
            return CameraCaptureIsTorchModeSupported(_nativePtr, torchMode);
        }

        /// <summary>
        /// Returns the physical orientation of the camera sensor.
        /// </summary>
        /// <remarks>
        /// </remarks>
        public int rotation
        {
            get
            {
                return CameraCaptureRotation(_nativePtr);
            }
        }

        /// <summary>
        /// Returns true when the given focus mode is supported.
        /// </summary>
        /// <param name="focusMode"></param>
        /// <returns></returns>
        public bool IsFocusModeSupported(FocusMode focusMode)
        {
            return CameraCaptureIsFocusModeSupported(_nativePtr, focusMode);
        }

        /// <summary>
        /// Gets or sets the focus mode.
        /// </summary>
        public FocusMode focusMode
        {
            get
            {
                return CameraCaptureGetFocusMode(_nativePtr);
            }
            set
            {
                CameraCaptureSetFocusMode(_nativePtr, value);
            }
        }

        /// <summary>
        /// Indicates whether the device supports a point of interest for focus.
        /// </summary>
        public bool isFocusPointOfInterestSupported
        {
            get
            {
                return CameraCaptureIsFocusPointOfInterestSupported(_nativePtr);
            }
        }

        /// <summary>
        /// Sets the point of interest for focusing.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool FocusPointOfInterest(Vector2 point)
        {
            return CameraCaptureFocusPointOfInterest(_nativePtr, ref point);
        }

        /// <summary>
        /// Indicates whether the device is currently adjusting its focus setting.
        /// </summary>
        /// <returns></returns>
        public bool isAdjustingFocus
        {
            get
            {
                return CameraCaptureIsAdjustingFocus(_nativePtr);
            }
        }

        /// <summary>
        /// Indicates whether the given white balance mode is supported.
        /// </summary>
        /// <param name="whiteBalanceMode"></param>
        /// <returns></returns>
        public bool IsWhiteBalanceModeSupported(WhiteBalanceMode whiteBalanceMode)
        {
            return CameraCaptureIsWhiteBalanceModeSupported(_nativePtr, whiteBalanceMode);
        }

        /// <summary>
        /// Gets or sets the current white balance mode.
        /// </summary>
        public WhiteBalanceMode whiteBalanceMode
        {
            get
            {
                return CameraCaptureGetWhiteBalanceMode(_nativePtr);
            }
            set
            {
                CameraCaptureSetWhiteBalanceMode(_nativePtr, value);
            }
        }

        /// <summary>
        /// Indicates whether the given exposure mode is supported.
        /// </summary>
        /// <param name="exposureMode"></param>
        /// <returns></returns>
        public bool IsExposureModeSupported(ExposureMode exposureMode)
        {
            return CameraCaptureIsExposureModeSupported(_nativePtr, exposureMode);
        }

        /// <summary>
        /// Gets or sets the exposure mode for the device.
        /// </summary>
        public ExposureMode exposureMode
        {
            get
            {
                return CameraCaptureGetExposureMode(_nativePtr);
            }
            set
            {
                CameraCaptureSetExposureMode(_nativePtr, value);
            }
        }

        /// <summary>
        ///  Flips the captured image horizontally.
        /// </summary>
        public bool horizontalFlip
        {
            get
            {
                return CameraCaptureHorizontalFlip(_nativePtr);
            }
            set
            {
                CameraCaptureSetHorizontalFlip(_nativePtr, value);
            }
        }

        /// <summary>
        /// Gets the number of cameras available on the system.
        /// </summary>
        public static int camerasCount
        {
            get
            {
                return CameraCaptureCamerasCount();
            }
        }

        /// <summary>
        /// Collects information about all camera devices available on the system.
        /// </summary>
        /// <remarks> When no cameras are connected this function will return an empty array.
        /// </remarks>
        public static CameraInfo[] availableCameras
        {
            get
            {
                int size = 0;
                IntPtr nativeCameraInfo = CameraCaptureAvailableCameras(ref size);
                CameraInfo[] info = new CameraInfo[size];

                for (int i = 0; i < size; ++i)
                {
                    CameraCaptureGetCameraInfoAt(nativeCameraInfo, ref info[i], i);
                }

                CameraCaptureDisposeCameraInfoArray(nativeCameraInfo);
                return info;
            }
        }

        /// <summary>
        /// Finds all capture formats supported by the selected device.
        /// </summary>
        /// <param name="deviceIndex"></param>
        /// <returns> The list of all formats supported by the selected device. </returns>
        /// <remarks> When the deviceIndex parameter value is higher than or equal to <see cref="camerasCount"/> 
        /// this function will return an empty array.
        /// </remarks>
        public static CaptureFormat[] FindSupportedFormats(uint deviceIndex)
        {

            int size = 0;
            IntPtr nativeFormats = CameraCaptureFindSupportedFormats(deviceIndex, ref size);
            CaptureFormat[] formats = new CaptureFormat[size];

            for (int i = 0; i < size; ++i)
            {
                CameraCaptureGetCaptureFormatAt(nativeFormats, ref formats[i], i);
            }

            CameraCaptureDisposeCaptureFormatArray(nativeFormats);
            return formats;
        }



        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureCreateReference", CallingConvention = Config.callConvention)]
        private static extern IntPtr CameraCaptureCreateReference();

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureDisposeReference", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureDisposeReference(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureOpen", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureOpen(IntPtr ptr, uint deviceIndex, uint width, uint height);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureClose", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureClose(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureGrab", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureGrab(IntPtr ptr, ref uint width, ref uint height, ref uint bytesPerPixel);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureIsOpened", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureIsOpened(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureReadFrame", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureReadFrame(IntPtr ptr, IntPtr pixels, ref uint bytesCount, ref uint width, ref uint height, ref uint bytesPerPixel, ref ulong timestamp);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureReadFrameToTexture2D", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureReadFrameToTexture2D(IntPtr ptr, IntPtr pixels);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureSetPixelsFormat", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureSetPixelsFormat(IntPtr ptr, PixelsFormat format);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureGetPixelsFormat", CallingConvention = Config.callConvention)]
        private static extern PixelsFormat CameraCaptureGetPixelsFormat(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureSetZoom", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureSetZoom(IntPtr ptr, float value);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureGetZoom", CallingConvention = Config.callConvention)]
        private static extern float CameraCaptureGetZoom(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureSetHorizontalFlip", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureSetHorizontalFlip(IntPtr ptr, bool value);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureHorizontalFlip", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureHorizontalFlip(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureIsZoomSupported", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureIsZoomSupported(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureMinZoom", CallingConvention = Config.callConvention)]
        private static extern float CameraCaptureMinZoom(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureMaxZoom", CallingConvention = Config.callConvention)]
        private static extern float CameraCaptureMaxZoom(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureSetTorchMode", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureSetTorchMode(IntPtr ptr, TorchMode torchMode);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureIsTorchModeSupported", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureIsTorchModeSupported(IntPtr ptr, TorchMode torchMode);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureGetTorchMode", CallingConvention = Config.callConvention)]
        private static extern TorchMode CameraCaptureGetTorchMode(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureSetTorchLevel", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureSetTorchLevel(IntPtr ptr, float torchLevel);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureIsTorchLevelSupported", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureIsTorchLevelSupported(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureIsTorchAvailable", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureIsTorchAvailable(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureRotation", CallingConvention = Config.callConvention)]
        private static extern int CameraCaptureRotation(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureIsFocusModeSupported", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureIsFocusModeSupported(IntPtr ptr, FocusMode focusMode);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureSetFocusMode", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureSetFocusMode(IntPtr ptr, FocusMode focusMode);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureGetFocusMode", CallingConvention = Config.callConvention)]
        private static extern FocusMode CameraCaptureGetFocusMode(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureIsFocusPointOfInterestSupported", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureIsFocusPointOfInterestSupported(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureFocusPointOfInterest", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureFocusPointOfInterest(IntPtr ptr, ref Vector2 point);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureIsAdjustingFocus", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureIsAdjustingFocus(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureIsWhiteBalanceModeSupported", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureIsWhiteBalanceModeSupported(IntPtr ptr, WhiteBalanceMode whiteBalanceMode);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureSetWhiteBalanceMode", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureSetWhiteBalanceMode(IntPtr ptr, WhiteBalanceMode whiteBalanceMode);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureGetWhiteBalanceMode", CallingConvention = Config.callConvention)]
        private static extern WhiteBalanceMode CameraCaptureGetWhiteBalanceMode(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureIsExposureModeSupported", CallingConvention = Config.callConvention)]
        private static extern bool CameraCaptureIsExposureModeSupported(IntPtr ptr, ExposureMode exposureMode);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureSetExposureMode", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureSetExposureMode(IntPtr ptr, ExposureMode exposureMode);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureGetExposureMode", CallingConvention = Config.callConvention)]
        private static extern ExposureMode CameraCaptureGetExposureMode(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureCamerasCount", CallingConvention = Config.callConvention)]
        private static extern int CameraCaptureCamerasCount();

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureAvailableCameras", CallingConvention = Config.callConvention)]
        private static extern IntPtr CameraCaptureAvailableCameras(ref int size);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureGetCameraInfoAt", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureGetCameraInfoAt(IntPtr list, ref CameraInfo item, int index);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureDisposeCameraInfoArray", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureDisposeCameraInfoArray(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureFindSupportedFormats", CallingConvention = Config.callConvention)]
        private static extern IntPtr CameraCaptureFindSupportedFormats(uint deviceIndex, ref int size);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureGetCaptureFormatAt", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureGetCaptureFormatAt(IntPtr list, ref CaptureFormat item, int index);

        [DllImport(Config.libraryPath, EntryPoint = "CameraCaptureDisposeCaptureFormatArray", CallingConvention = Config.callConvention)]
        private static extern void CameraCaptureDisposeCaptureFormatArray(IntPtr ptr);

    }
}
