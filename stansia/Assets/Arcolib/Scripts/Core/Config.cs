﻿

using System.Runtime.InteropServices;

namespace Vexpot.Arcolib
{
    /// <summary>
    /// Defines multi-platform configurations.
    /// </summary>
    static public class Config
    {

        /// <summary>
        ///  Maximum number of symbols per frame. If you define a higher value in <see cref="TrackerOptions.maxSymbolsPerFrame"/> that value 
        ///  is overrode by the value defined here. You must not change this value due is constrained to the internal core.
        /// </summary>
        public const int SYMBOL_FRAME_MAX_SYMBOLS = 10;

        /// <summary>
        /// Specifies the calling convention required to call methods from library's core.
        /// </summary>
        public const CallingConvention callConvention = CallingConvention.Cdecl;

        /// <summary>
        /// Native library binding name.
        /// </summary>
#if (UNITY_WEBGL || UNITY_IOS) && !UNITY_EDITOR
             public const string libraryPath = "__Internal";
#elif UNITY_ANDROID || UNITY_STANDALONE || UNITY_WINRT || UNITY_EDITOR
        public const string libraryPath = "arco";
        #endif

    }
}
