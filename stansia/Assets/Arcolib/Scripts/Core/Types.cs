﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Vexpot.Arcolib
{

    /// <summary>
    ///  Defines all <see cref="ISymbol"/> tracking states. Tracking states are useful 
    ///  to determine when a symbol is ready for certain actions.
    /// </summary>
    public enum TrackingState
    {
        /// <summary>
        /// The <see cref="ISymbol"/> is waiting to be processed. 
        /// When symbols are in this state you must not use them.
        /// </summary>
        Idle,
        /// <summary>
        /// The <see cref="ISymbol"/> has been detected and is pending to be tracked in the 
        /// next frame. If the symbol is lost in the next frame then it turns its state to <see cref="Idle"/>.
        /// In this state the symbols are ready to be used but wont have persistence among frames.
        /// </summary>
        Detected,
        /// <summary>
        /// The <see cref="ISymbol"/> is being tracked and ready to be used for multi-frame representation.
        /// </summary>
        Tracked
    }

    /// <summary>
    /// Specifies the exposure modes of a capture device.
    /// </summary>
    public enum ExposureMode
    {
        /// <summary>
        /// The exposure is locked.
        /// </summary>
        Locked,
        /// <summary>
        /// The device automatically adjusts the exposure once.
        /// </summary>
        Auto,
        /// <summary>
        /// The device continuously monitors exposure levels and auto exposes when necessary.
        /// </summary>
        ContinuousAuto,
        /// <summary>
        /// The exposure mode for this device is unavailable.
        /// </summary>
        Unspecified = 0x20
	}

    /// <summary>
    /// Specifies the focus modes of a capture device.
    /// </summary>
    public enum FocusMode
    {
        /// <summary>
        /// The focus is locked.
        /// </summary>
        Locked,
        /// <summary>
        /// The device will automatically adjusts the focus once.
        /// </summary>
        Auto,
        /// <summary>
        /// The device continuously monitors focus and auto focuses when necessary.
        /// </summary>
        ContinuousAuto,
        /// <summary>
        /// Onetime shot auto focus to objects close to camera.
        /// </summary>
        Macro
    }

    /// <summary>
    /// Specifies the white balance modes of a capture device.
    /// </summary>
    public enum WhiteBalanceMode
    {
        /// <summary>
        /// The white balance setting is locked.
        /// </summary>
        Locked,
        /// <summary>
        /// The device performs an auto white balance operation once.
        /// </summary>
        Auto,
        /// <summary>
        /// The device continuously monitors white balance and adjusts when necessary.
        /// </summary>
        ContinuousAuto,
        /// <summary>
        /// The white balance setting for this device is unavailable.
        /// </summary>
        Unspecified = 0x20
	}

    /// <summary>
    /// Specifies the format of the color data for each pixel in the image.
    /// </summary>
    public enum PixelsFormat
    {
        /// <summary>
        /// The pixel format is invalid.
        /// </summary>
        Invalid,
        /// <summary>
        /// Specifies that the format is 24 bits per pixel. 8 bits are used for the blue, green, and red components.
        /// </summary>
        BGR24,
        /// <summary>
        /// Specifies that the format is 24 bits per pixel. 8 bits are used for the red, green, and blue components.
        /// </summary>
        RGB24,
        /// <summary>
        /// Specifies that the format is 32 bits per pixel. 8 bits are used for the blue, green, red and alpha components.
        /// </summary>
        BGRA32,
        /// <summary>
        /// Specifies that the format is 32 bits per pixel. 8 bits are used for the red, green, blue and alpha components.
        /// </summary>
        RGBA32,
        /// <summary>
        /// The pixel format is 8 bits per pixel.
        /// </summary>
        Grayscale
    }

    /// <summary>
    /// Specifies the physical position of the camera on the system.
    /// </summary>
    public enum CameraPosition
    {
        /// <summary>
        /// The camera sensor is on the back.
        /// </summary>
        BackFace,
        /// <summary>
        /// The camera sensor is on the front.
        /// </summary>
        FrontFace,
        /// <summary>
        /// The camera position relative to the system hardware is unspecified or unknown.
        /// </summary>
        Unspecified = 0x20
    }

    /// <summary>
    /// Constants to specify the capture device torch modes.
    /// </summary>
    public enum TorchMode
    {
        /// <summary>
        /// The capture device torch is always off.
        /// </summary>
        Off,
        /// <summary>
        /// The capture device torch is always on.
        /// </summary>
        On,
        /// <summary>
        /// The capture device continuously monitors light levels and uses the torch when necessary.
        /// </summary>
        Auto
    }

    /// <summary>
    /// Encapsulates the data in a captured frame.
    /// </summary>
    public struct ImageFrame
    {
        /// <summary>
        /// 
        /// </summary>
        public uint width;
        /// <summary>
        /// 
        /// </summary>
        public uint height;
        /// <summary>
        /// 
        /// </summary>
        public uint bpp;
        /// <summary>
        /// 
        /// </summary>
        public uint bytesCount;
        /// <summary>
        /// 
        /// </summary>
        public ulong timestamp;
        /// <summary>
        /// 
        /// </summary>
        public byte[] data;
    }

    /// <summary>
    /// Defines a capture format supported by the device.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CaptureFormat
    {
        /// <summary>
        /// 
        /// </summary>
        public uint width;
        /// <summary>
        /// 
        /// </summary>
        public uint height;
        /// <summary>
        /// 
        /// </summary>
        public uint frameRate;

        /// <summary>
        /// Returns the string representation of the object.
        /// </summary>
        /// <returns></returns>
        override public string ToString()
        {
            return "[" + width.ToString() + "x" + height.ToString() + "@" + frameRate.ToString() + "fps]";
        }
    };

    /// <summary>
    /// Provides general information about camera devices.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CameraInfo
    {
        /// <summary>
        /// The device name of the camera.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string deviceName;
        /// <summary>
        /// Specifies whether the device has a torch.
        /// </summary>
        public bool hasTorch;
        /// <summary>
        /// Returns the physical position of the camera on the hardware system.
        /// </summary>
        public CameraPosition position;

        /// <summary>
        /// Returns the string representation of the object.
        /// </summary>
        /// <returns></returns>
        override public string ToString()
        {
            return "[name: " + deviceName + " hasTorch: " + hasTorch.ToString() + " position: " + position.ToString() +"]";
        }
    }
}
