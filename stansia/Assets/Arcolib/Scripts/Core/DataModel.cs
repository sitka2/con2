﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Vexpot.Arcolib
{
    /// <summary>
    /// Inside Arcolib data models are optimized structures that contains certain amount of information
    /// describing patterns that allows trackers to detect and tracks symbols faster and precisely. 
    /// </summary>
    /// <remarks>
    /// This class contains all generic information needed by all types of data 
    /// models like <see cref="BarcodeModel"/> and <see cref="FeaturesModel"/>.
    /// </remarks>   
    public abstract class DataModel
    {
        /// <summary>
        ///  Default constructor.
        /// </summary>
        public DataModel()
        {
            _nativeDataModelPtr = IntPtr.Zero;
        }

        /// <summary>
        /// An integer that you can use to identify model objects in your application.
        /// </summary>
        public uint tag { get; set; }

        /// <summary>
        /// Pointer to the native <see cref="DataModel"/> object. 
        /// This will hold the memory address of the 
        /// unmanaged objects references in C++ core. 
        /// Usually you will not have to deal with this.
        /// </summary>
        internal IntPtr _nativeDataModelPtr;
    }

    /// <summary>
    /// Contains training data used to detect faces and landmarks.
    /// </summary>
    public class FaceModel : DataModel
    {
        /// <summary>
        /// Creates a new <see cref="FaceModel"/> instance.
        /// </summary>
        public FaceModel() : base()
        {
            _nativeDataModelPtr = FaceModelCreateReference();
        }

        /// <summary>
        /// Used to dispose native memory.
        /// </summary>
        ~FaceModel()
        {
            FaceModelDisposeReference(_nativeDataModelPtr);
        }

        /// <summary>
        /// Reads a face model from Unity <see cref="TextAsset"/>.
        /// </summary>
        /// <param name="data">The model binary data.</param>
        /// <returns>True if the model was successfully read.</returns>
        public bool Read(TextAsset data)
        {
            bool result = false;
            try
            {
                if (data != null)
                    result = FaceModelRead(_nativeDataModelPtr, data.bytes);               
            }
            catch
            {
                throw new ArgumentException("Native FaceModel.read unhandled exception.");
            }
            return result;
        }

        /// <summary>
        /// Loads an existing face model from disk.
        /// </summary>
        /// <param name="filename">The face model filename.</param>
        /// <returns>True if the model was successfully loaded.</returns>
        public bool Load(string filename)
        {
            bool result = false;
            try
            {
                if (File.Exists(filename))
                    result = FaceModelLoad(_nativeDataModelPtr, filename);
            }
            catch
            {
                throw new InvalidOperationException("Native FaceModel.Load has failed.");
            }
            return result;
        }

        [DllImport(Config.libraryPath, EntryPoint = "FaceModelCreateReference", CallingConvention = Config.callConvention)]
        private static extern IntPtr FaceModelCreateReference();

        [DllImport(Config.libraryPath, EntryPoint = "FaceModelDisposeReference", CallingConvention = Config.callConvention)]
        private static extern void FaceModelDisposeReference(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FaceModelLoad", CallingConvention = Config.callConvention)]
        private static extern bool FaceModelLoad(IntPtr ptr, string filename);

        [DllImport(Config.libraryPath, EntryPoint = "FaceModelRead", CallingConvention = Config.callConvention)]
        private static extern bool FaceModelRead(IntPtr ptr, byte[] data);
    }

        /// <summary>
        /// Encodes information in a two dimensional barcode representation.
        /// This class is meant to be a generic model of all kind of barcodes.
        /// </summary>
        public class BarcodeModel : DataModel
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BarcodeModel() : base()
        {
            _nativeDataModelPtr = BarcodeModelCreateReference();
        }

        /// <summary>
        /// Used to dispose all unmanaged memory allocated.
        /// </summary>
        ~BarcodeModel()
        {
            BarcodeModelDisposeReference(_nativeDataModelPtr);
        }

        /// <summary>
        ///  Save the model to disk. You must call <see cref="Create(uint, int, bool)"/> before calling this
        ///  in order to store a valid model.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool Save(string filename)
        {
            return BarcodeModelSave(_nativeDataModelPtr, filename);
        }

        /// <summary>
        /// Builds a new <see cref="BarcodeModel"/> using the defined attributes.
        /// </summary>
        /// <param name="id">The identifier that you want to encode in the model</param>
        /// <param name="size">The model resolution in pixels</param>
        /// <param name="withColors">If true the model will be generated using color encoding.</param>
        public void Create(uint id, int size, bool withColors)
        {
            BarcodeModelCreate(_nativeDataModelPtr, id, size, withColors);
        }

        /// <summary>
        /// Builds a new <see cref="BarcodeModel"/> using the defined attributes.
        /// Use this to build QR codes.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="size"></param>
        public void Create(string data, int size)
        {
            BarcodeModelCreate2(_nativeDataModelPtr, data, size);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Texture2D GetImage()
        {
            int w = 0, h = 0, dataSize = 0;
            IntPtr data = BarcodeModelGetRawImage(_nativeDataModelPtr, ref w, ref h, ref dataSize);
            Texture2D image = new Texture2D(w, h, TextureFormat.RGB24, false);
            image.LoadRawTextureData(data, dataSize);
            image.Apply(false);
            return image;
        }

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeModelCreateReference", CallingConvention = Config.callConvention)]
        private static extern IntPtr BarcodeModelCreateReference();

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeModelCreate", CallingConvention = Config.callConvention)]
        private static extern void BarcodeModelCreate(IntPtr ptr, uint id, int size, bool withColors);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeModelCreate2", CallingConvention = Config.callConvention)]
        private static extern void BarcodeModelCreate2(IntPtr ptr, string data, int size);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeModelSave", CallingConvention = Config.callConvention)]
        private static extern bool BarcodeModelSave(IntPtr ptr, string filename);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeModelDisposeReference", CallingConvention = Config.callConvention)]
        private static extern void BarcodeModelDisposeReference(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "BarcodeModelGetRawImage", CallingConvention = Config.callConvention)]
        private static extern IntPtr BarcodeModelGetRawImage(IntPtr ptr, ref int width, ref int height, ref int dataSize);       
    }

    /// <summary>
    /// Contains keypoints and descriptors used to recognize images.
    /// </summary>
    /// <remarks>
    /// This class also provide some useful methods to build, load and save the models.
    /// </remarks>
    public class FeaturesModel : DataModel
    {
        /// <summary>
        /// Creates a new <see cref="FeaturesModel"/> instance.
        /// </summary>
        public FeaturesModel() : base()
        {
            _nativeDataModelPtr = FeaturesModelCreateReference();
        }

        /// <summary>
        /// Used to dispose native memory.
        /// </summary>
        ~FeaturesModel()
        {
            FeaturesModelDisposeReference(_nativeDataModelPtr);
        }

        /// <summary>
        /// Gets the model quality level. 
        /// </summary>
        /// <remarks>
        /// The quality means how good 
        /// is the image to be recognized over heterogeneous scenarios.
        /// </remarks>
        public float qualityLevel
        {
            get { return FeaturesModelGetQualityLevel(_nativeDataModelPtr); }
        }

        /// <summary>
        /// Enables or disables the model.
        /// </summary>
        ///  <remarks>
        ///  When a model is disabled the tracker will skip it during detection phase.
        ///  During a game you may want to enable or disable models 
        ///  according to the game level or achievements. 
        ///  </remarks>
        public bool enabled
        {
            get { return FeaturesModelGetEnabled(_nativeDataModelPtr); }
            set { FeaturesModelSetEnabled(_nativeDataModelPtr, value); }
        }

        /// <summary>
        /// Save the model to disk. 
        /// You must call <see cref="FeaturesModel.Create(string)"/> or 
        /// <see cref="FeaturesModel.Create(Texture2D, Rect)"/> 
        /// before calling this in order to store a valid model.
        /// </summary>
        /// <param name="filename">The location to store the model.</param>
        /// <returns>True if the model was successfully saved.</returns>
        public bool Save(string filename)
        {
            bool result = false;
            try
            {
                result = FeaturesModelSave(_nativeDataModelPtr, filename);
            }
            catch
            {
                throw new ArgumentException("Native FeaturesModel.save unhandled exception.");
            }
            return result;
        }

        /// <summary>
        /// Reads a features model from Unity <see cref="TextAsset"/>.
        /// </summary>
        /// <param name="data">The model binary data.</param>
        /// <returns>True if the model was successfully read.</returns>
        public bool Read(TextAsset data)
        {
            bool result = false;
            try
            {
                if (data != null)
                    result = FeaturesModelRead(_nativeDataModelPtr, data.bytes);
            }
            catch
            {
                throw new ArgumentException("Native FeaturesModel.read unhandled exception.");
            }
            return result;
        }

        /// <summary>
        /// Loads an existing features model from disk.
        /// </summary>
        /// <param name="filename">The features model filename.</param>
        /// <returns>True if the model was successfully loaded.</returns>
        public bool Load(string filename)
        {
            bool result = false;
            try
            {
                if (File.Exists(filename))
                    result = FeaturesModelLoad(_nativeDataModelPtr, filename);
            }
            catch
            {
                throw new InvalidOperationException("FeaturesModel.Load has failed.");
            }
            return result;
        }

        /// <summary>
        /// Creates a <see cref="FeaturesModel"/> from local image on disk.
        /// </summary>
        /// <param name="imageFilePath">The image file in JPG or PNG format.</param>
        public void Create(string imageFilePath)
        {
            try
            {
                if (File.Exists(imageFilePath))
                    FeaturesModelCreate(_nativeDataModelPtr, imageFilePath);
            }
            catch
            {
                throw new InvalidOperationException("FeaturesModel.Create has failed.");
            }
        }


        /// <summary>
        /// Creates a <see cref="FeaturesModel"/> from texture data.
        /// </summary>
        /// <remarks>
        /// Texture must be in RGB24 or RGBA32 formats.
        /// </remarks>
        /// <param name="image">The image used to build the model.</param>
        /// <param name="regionOfInterest">Optionally you can build the model with only a region of the image. The <see cref="Rect"/>
        /// coordinates must be inside the bounds of the image. </param>
        public void Create(Texture2D image, Rect regionOfInterest = new Rect())
        {
            try
            {
                if (image.format == TextureFormat.RGBA32 || image.format == TextureFormat.RGB24)
                {
                    int bpp = (int)image.format;
                    FeaturesModelCreate2(_nativeDataModelPtr, image.GetRawTextureData(), image.width, image.height, bpp, ref regionOfInterest);
                }
                else throw new Exception();
            }
            catch
            {
                throw new InvalidOperationException("Only TextureFormat.RGBA32 and TextureFormat.RGB24 formats are supported.");
            }
        }

        /// <summary>
        /// Draws the extracted keypoints over the image.
        /// </summary>
        /// <param name="image">The image to detect keypoints.</param>
        public void PlotKeyPoints(Texture2D image)
        {
            try
            {
                if (image && (image.format == TextureFormat.RGBA32 || image.format == TextureFormat.RGB24))
                {
                    byte[] data = image.GetRawTextureData();

                    FeaturesModelPlotKeyPoints(_nativeDataModelPtr, data,
                        image.width,
                        image.height,
                        (int)image.format);

                   image.LoadRawTextureData(data);
                   image.Apply(false);                    
                }
            }

            catch
            {
                throw new InvalidOperationException("PlotKeyPoints has failed!");
            }            
        }

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelCreateReference", CallingConvention = Config.callConvention)]
        private static extern IntPtr FeaturesModelCreateReference();

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelDisposeReference", CallingConvention = Config.callConvention)]
        private static extern void FeaturesModelDisposeReference(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelCreate", CallingConvention = Config.callConvention)]
        private static extern void FeaturesModelCreate(IntPtr ptr, string image);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelCreate2", CallingConvention = Config.callConvention)]
        private static extern void FeaturesModelCreate2(IntPtr ptr, byte[] imageData, int width, int height, int bpp, ref Rect roi);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelSetEnabled", CallingConvention = Config.callConvention)]
        private static extern void FeaturesModelSetEnabled(IntPtr ptr, bool enabled);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelGetEnabled", CallingConvention = Config.callConvention)]
        private static extern bool FeaturesModelGetEnabled(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelSave", CallingConvention = Config.callConvention)]
        private static extern bool FeaturesModelSave(IntPtr ptr, string filename);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelLoad", CallingConvention = Config.callConvention)]
        private static extern bool FeaturesModelLoad(IntPtr ptr, string filename);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelRead", CallingConvention = Config.callConvention)]
        private static extern bool FeaturesModelRead(IntPtr ptr, byte[] data);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelGetQualityLevel", CallingConvention = Config.callConvention)]
        private static extern float FeaturesModelGetQualityLevel(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "FeaturesModelPlotKeyPoints", CallingConvention = Config.callConvention)]
        private static extern void FeaturesModelPlotKeyPoints(IntPtr ptr, byte[] data, int width, int height, int bytesPerPixel);
    }
}
