﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Vexpot.Arcolib
{
    /// <summary>
    ///  Defines a common protocol used by all symbols in the library.
    /// </summary>
    /// <remarks>
    /// In Arcolib symbols are the resulting set of data produced by tracking analysis. 
    /// Trackers will emit specialized symbols with useful information to the user. 
    /// Symbols are meant to be used as read-only data structures.
    /// </remarks>
    public interface ISymbol
    {
        /// <summary>
        /// This value is the unique and persistent ID for a detected symbol.
        /// </summary>
        uint id { get; }
        /// <summary>
        /// Contains the estimated pose 3D of this symbol. 
        /// </summary>
        Matrix4x4 transformMatrix { get; }
        /// <summary>
        /// Contains the current <see cref="TrackingState"/> for this symbol.
        /// </summary>
        TrackingState trackingState { get; }
    }

    /// <summary>
    /// Contains a collection of symbols. 
    /// </summary>
    /// <typeparam name="SymbolType">The symbol type to store on this collection.</typeparam>
    public interface ISymbolFrame<SymbolType> : IEnumerable<SymbolType>
    {
        /// <summary>
        /// Used to provide access to certain symbol at index. 
        /// </summary>
        /// <param name="index"> The symbol position inside the collection. </param>
        /// <returns></returns>
        SymbolType this[int index] { get; }
    }   

    /// <summary>
    /// Some useful functions to convert matrix data to Unity structures.
    /// </summary>
    public static class Matrix4x4Extensions
    {
        ///  <summary>
        ///  Copies the transformation matrix to the <see cref="Transform"/> object.
        ///  </summary>
        ///  <remarks>
        ///  This function is useful to render a <see cref="GameObject"/> in the 3D space.
        ///  </remarks>
        /// <param name="m"></param>
        /// <param name="transform"></param>
        /// <param name="applyOrientation"></param>
        public static void CopyTo(this Matrix4x4 m, Transform transform, bool applyOrientation = true)
        {
            if (applyOrientation)
            {
                Quaternion orientation = Quaternion.identity;
                MatrixOrientationToQuaternion(ref m, ref orientation);
                transform.rotation = orientation;
            }
            transform.position = m.GetColumn(3);
        }

        /// <summary>     
        ///  Computes the orientation quaternion from a <see cref="Matrix4x4"/>.  
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dst"></param>
        [DllImport(Config.libraryPath, EntryPoint = "MatrixOrientationToQuaternion", CallingConvention = Config.callConvention)]
        private static extern void MatrixOrientationToQuaternion(ref Matrix4x4 src, ref Quaternion dst);
    }

    /// <summary>
    ///  Represents an abstraction of any natural image detected.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct FeaturesSymbol : ISymbol
    {
        /// <summary>
        /// 
        /// </summary>
        private uint _id;

        /// <summary>
        ///  Contains the current <see cref="TrackingState"/> for this symbol.
        /// </summary>
        private TrackingState _trackingState;

        /// <summary>
        ///  Contains the estimated 2D centroid of this symbol in projective coordinates space.
        /// </summary>
        public Vector2 center;

        /// <summary>
        ///  Gets the symbol corners in projective image coordinates.
        /// </summary>
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 4, ArraySubType = UnmanagedType.Struct)]
        public Vector2[] corners;

        ///  <summary>
        ///  Contains the estimated pose 3D of this symbol. 
        ///  </summary>
        ///  <remarks>
        ///  The transformation matrix is computed internally by trackers
        ///  using the <see cref="DeviceCalibration"/> attached to
        ///  them.
        ///  </remarks>
        private Matrix4x4 _transformMatrix;

        /// <summary>
        /// Performs a deep copy of the <see cref="FeaturesSymbol"/>.
        /// Use this function wisely and only when you 
        /// really needed to avoid unnecessary memory allocation.
        /// </summary>
        /// <returns></returns>
        public FeaturesSymbol Clone()
        {
            FeaturesSymbol copy = new FeaturesSymbol();
            FeaturesSymbolCopyTo(ref this, ref copy);
            return copy;
        }

        /// <summary>
        /// This value is the unique and persistent ID for a detected symbol.
        /// </summary>
        public uint id
        {
            get
            { return _id; }
        }

        /// <summary>
        ///  Contains the current <see cref="TrackingState"/> for this symbol.
        /// </summary>
        public TrackingState trackingState
        {
            get { return _trackingState; }
        }

        ///  <summary>
        ///  Contains the estimated pose 3D of this symbol. 
        ///  </summary>
        ///  <remarks>
        ///  The transformation matrix is computed internally by trackers
        ///  using the <see cref="DeviceCalibration"/> attached to
        ///  them.
        ///  </remarks>
        public Matrix4x4 transformMatrix
        {
            get { return _transformMatrix; }
        }

        /// <summary>
        /// Performs a fast copy of <see cref="FeaturesSymbol"/> content.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dst"></param>
        [DllImport(Config.libraryPath, EntryPoint = "FeaturesSymbolCopyTo", CallingConvention = Config.callConvention)]
        private static extern void FeaturesSymbolCopyTo(ref FeaturesSymbol src, ref FeaturesSymbol dst);
    }


    /// <summary>
    /// This object represents an snapshot of a single frame produced 
    /// during <see cref="FeaturesTracker"/> detection/tracking process. 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct FeaturesSymbolFrame : ISymbolFrame<FeaturesSymbol>
    {
        /// <summary>
        /// Holds the frame number generated sequentially by the tracker that produces this frame. 
        /// </summary>
        /// <remarks>
        /// One tracker will always assign an unique number to each frame and you can use that number
        /// as an identifier for the frame. Be aware that if you restart the <see cref="FeaturesTracker"/>
        /// then the number sequence starts over. 
        /// </remarks>
        public int frameNumber;

        /// <summary>
        /// Provides the timestamp (in milliseconds) when the frame was created. 
        /// This value can be used to compute time deltas between frames.
        /// </summary>
        public long timestamp;

        /// <summary>
        /// The amount of valid <see cref="FeaturesSymbol"/>s stored on this frame.
        /// </summary>
        public int symbolCount;

        /// <summary>
        /// Internal symbols array.
        /// </summary>
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = Config.SYMBOL_FRAME_MAX_SYMBOLS, ArraySubType = UnmanagedType.Struct)]
        private FeaturesSymbol[] symbols;

        /// <summary>
        /// Subscript operator that ensures to get always valid symbols. 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public FeaturesSymbol this[int index]
        {
            get
            {
                if (index < symbolCount)
                    return symbols[index];
                else
                    throw new IndexOutOfRangeException("Index must never be greater or equal than symbolCount");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FeaturesSymbol> GetEnumerator()
        {
            for (int i = 0; i < symbolCount; i++)
                yield return this[i];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return symbols.GetEnumerator();
        }

        /// <summary>
        /// Performs a deep copy of the <see cref="FeaturesSymbolFrame"/>.
        /// </summary>
        /// <remarks>
        /// Use this function wisely and only when you 
        /// really needed to avoid unnecessary memory allocation.
        /// </remarks>
        /// <returns>An identical copy of this object.</returns>
        public FeaturesSymbolFrame Clone()
        {
            FeaturesSymbolFrame copy = new FeaturesSymbolFrame();
            FeaturesSymbolFrameCopyTo(ref this, ref copy);
            return copy;
        }

        /// <summary>
        /// Performs a fast copy of <see cref="FeaturesSymbolFrame"/> content.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dst"></param>
        [DllImport(Config.libraryPath, EntryPoint = "FeaturesSymbolFrameCopyTo", CallingConvention = Config.callConvention)]
        private static extern void FeaturesSymbolFrameCopyTo(ref FeaturesSymbolFrame src, ref FeaturesSymbolFrame dst);
    }


    /// <summary>
    ///  Represents an abstraction of any barcode object supported by the library. 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BarcodeSymbol : ISymbol
    {
        /// <summary>
        /// 
        /// </summary>
        private uint _id;

        /// <summary>
        /// 
        /// </summary>
        private TrackingState _trackingState;

        /// <summary>
        ///  Contains the estimated 2D centroid of this symbol in projective coordinates space.
        /// </summary>
        public Vector2 center;

        /// <summary>
        ///  Gets the symbol corners in projective image coordinates.
        /// </summary>
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 4, ArraySubType = UnmanagedType.Struct)]
        public Vector2[] corners;

        /// <summary>
        /// 
        /// </summary>
        private Matrix4x4 _transformMatrix;

        /// <summary>
        ///  Contains additional encoded information. 
        /// </summary>
        /// <remarks>
        /// For certain barcode types this information is only the id. 
        /// The string max size is up to 1024 characters.
        /// </remarks>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1024)]
        public string data;

        /// <summary>
        /// Performs a deep copy of the <see cref="BarcodeSymbol"/>.
        /// Use this function wisely and only when you 
        /// really needed to avoid unnecessary memory allocation.
        /// </summary>
        /// <returns></returns>
        public BarcodeSymbol Clone()
        {
            BarcodeSymbol copy = new BarcodeSymbol();
            BarcodeSymbolCopyTo(ref this, ref copy);
            return copy;
        }

        /// <summary>
        /// This value is the unique and persistent ID for a detected symbol.
        /// </summary>
        public uint id
        {
            get
            { return _id; }
        }

        /// <summary>
        ///  Contains the current <see cref="TrackingState"/> for this symbol.
        /// </summary>
        public TrackingState trackingState
        {
            get { return _trackingState; }
        }

        ///  <summary>
        ///  Contains the estimated pose 3D of this symbol. 
        ///  </summary>
        ///  <remarks>
        ///  The transformation matrix is computed internally by trackers
        ///  using the <see cref="DeviceCalibration"/> attached to
        ///  them.
        ///  </remarks>
        public Matrix4x4 transformMatrix
        {
            get { return _transformMatrix; }
        }

        /// <summary>
        /// Performs a fast copy of <see cref="BarcodeSymbol"/> content.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dst"></param>
        [DllImport(Config.libraryPath, EntryPoint = "BarcodeSymbolCopyTo", CallingConvention = Config.callConvention)]
        private static extern void BarcodeSymbolCopyTo(ref BarcodeSymbol src, ref BarcodeSymbol dst);
    }

    /// <summary>
    /// This object represents an snapshot of a single frame produced 
    /// during <see cref="BarcodeTracker"/> detection/tracking process. 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BarcodeSymbolFrame : ISymbolFrame<BarcodeSymbol>
    {
        /// <summary>
        /// Holds the frame number generated sequentially by the tracker that produces this frame. 
        /// </summary>
        /// <remarks>
        /// One tracker will always assign an unique number to each frame and you can use that number
        /// as an identifier for the frame. Be aware that if you restart the <see cref="BarcodeTracker"/>
        /// then the number sequence starts over. 
        /// </remarks>
        public int frameNumber;

        /// <summary>
        /// Provides the timestamp (in milliseconds) when the frame was created. 
        /// This value can be used to compute time deltas between frames.
        /// </summary>
        public long timestamp;

        /// <summary>
        /// The amount of valid <see cref="BarcodeSymbol"/>s stored on this frame.
        /// </summary>
        public int symbolCount;

        /// <summary>
        /// Internal symbols array.
        /// </summary>
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = Config.SYMBOL_FRAME_MAX_SYMBOLS, ArraySubType = UnmanagedType.Struct)]
        private BarcodeSymbol[] symbols;

        /// <summary>
        /// Subscript operator that ensures to get always valid symbols. 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public BarcodeSymbol this[int index]
        {
            get
            {
                if (index < symbolCount)
                    return symbols[index];
                else
                    throw new IndexOutOfRangeException("Index must never be greater or equal than symbolCount");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<BarcodeSymbol> GetEnumerator()
        {
            for (int i = 0; i < symbolCount; i++)
                yield return this[i];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return symbols.GetEnumerator();
        }

        /// <summary>
        /// Performs a deep copy of the <see cref="BarcodeSymbolFrame"/>.
        /// </summary>
        /// <remarks>
        /// Use this function wisely and only when you 
        /// really needed to avoid unnecessary memory allocation.
        /// </remarks>
        /// <returns>An identical copy of this object.</returns>
        public BarcodeSymbolFrame Clone()
        {
            BarcodeSymbolFrame copy = new BarcodeSymbolFrame();
            BarcodeSymbolFrameCopyTo(ref this, ref copy);
            return copy;
        }

        /// <summary>
        /// Performs a fast copy of <see cref="BarcodeSymbolFrame"/> content.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dst"></param>
        [DllImport(Config.libraryPath, EntryPoint = "BarcodeSymbolFrameCopyTo", CallingConvention = Config.callConvention)]
        private static extern void BarcodeSymbolFrameCopyTo(ref BarcodeSymbolFrame src, ref BarcodeSymbolFrame dst);
    }

    /// <summary>
    ///  Represents an abstraction of any face detected. 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct FaceSymbol : ISymbol
    {
        /// <summary>
        /// 
        /// </summary>
        private uint _id;

        /// <summary>
        /// 
        /// </summary>
        private TrackingState _trackingState;

        /// <summary>
        ///  Contains the estimated 2D centroid of this symbol in projective coordinates space.
        /// </summary>
        public Vector2 center;

        /// <summary>
        /// 
        /// </summary>
        private Matrix4x4 _transformMatrix;

        /// <summary>
        /// Performs a deep copy of the <see cref="FaceSymbol"/>.      
        /// </summary>
        /// <remarks>
        /// Use this function wisely and only when you 
        /// really needed to avoid unnecessary memory allocation.
        /// </remarks>
        /// <returns>The clone of this symbol.</returns>
        public FaceSymbol Clone()
        {
            FaceSymbol copy = new FaceSymbol();
            FaceSymbolCopyTo(ref this, ref copy);
            return copy;
        }

        /// <summary>
        /// This value is the unique and persistent ID for a detected symbol.
        /// </summary>
        public uint id
        {
            get
            { return _id; }
        }

        /// <summary>
        ///  Contains the current <see cref="TrackingState"/> for this symbol.
        /// </summary>
        public TrackingState trackingState
        {
            get { return _trackingState; }
        }

        ///  <summary>
        ///  Contains the estimated pose 3D of this symbol. 
        ///  </summary>
        ///  <remarks>
        ///  The transformation matrix is computed internally by trackers
        ///  using the <see cref="DeviceCalibration"/> attached to
        ///  them.
        ///  </remarks>
        public Matrix4x4 transformMatrix
        {
            get { return _transformMatrix; }
        }

        /// <summary>
        /// Performs a fast copy of <see cref="FaceSymbol"/> content.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dst"></param>
        [DllImport(Config.libraryPath, EntryPoint = "FaceSymbolCopyTo", CallingConvention = Config.callConvention)]
        private static extern void FaceSymbolCopyTo(ref FaceSymbol src, ref FaceSymbol dst);
    }

    /// <summary>
    /// This object represents an snapshot of a single frame produced 
    /// during the <see cref="FaceTracker"/> analysis. 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct FaceSymbolFrame : ISymbolFrame<FaceSymbol>
    {
        /// <summary>
        /// Holds the frame number generated sequentially by the tracker that produces this frame. 
        /// </summary>
        /// <remarks>
        /// One tracker will always assign an unique number to each frame and you can use that number
        /// as an identifier for the frame. Be aware that if you restart the <see cref="FaceTracker"/>
        /// then the number sequence starts over. 
        /// </remarks>
        public int frameNumber;

        /// <summary>
        /// Provides the timestamp (in milliseconds) when the frame was created. 
        /// This value can be used to compute time deltas between frames.
        /// </summary>
        public long timestamp;

        /// <summary>
        /// The amount of valid <see cref="FaceSymbol"/>s stored on this frame.
        /// </summary>
        public int symbolCount;

        /// <summary>
        /// Internal symbols array.
        /// </summary>
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = Config.SYMBOL_FRAME_MAX_SYMBOLS, ArraySubType = UnmanagedType.Struct)]
        private FaceSymbol[] symbols;

        /// <summary>
        /// Subscript operator that ensures to get always valid symbols. 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public FaceSymbol this[int index]
        {
            get
            {
                if (index < symbolCount)
                    return symbols[index];
                else
                    throw new IndexOutOfRangeException("Index must never be greater or equal than symbolCount");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FaceSymbol> GetEnumerator()
        {
            for (int i = 0; i < symbolCount; i++)
                yield return this[i];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return symbols.GetEnumerator();
        }

        /// <summary>
        /// Performs a deep copy of the <see cref="FaceSymbolFrame"/>.
        /// </summary>
        /// <remarks>
        /// Use this function wisely and only when you 
        /// really needed to avoid unnecessary memory allocation.
        /// </remarks>
        /// <returns>An identical copy of this object.</returns>
        public FaceSymbolFrame Clone()
        {
            FaceSymbolFrame copy = new FaceSymbolFrame();
            FaceSymbolFrameCopyTo(ref this, ref copy);
            return copy;
        }

        /// <summary>
        /// Performs a fast copy of <see cref="FaceSymbolFrame"/> content.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dst"></param>
        [DllImport(Config.libraryPath, EntryPoint = "FaceSymbolFrameCopyTo", CallingConvention = Config.callConvention)]
        private static extern void FaceSymbolFrameCopyTo(ref FaceSymbolFrame src, ref FaceSymbolFrame dst);
    }
}
