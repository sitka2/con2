﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Vexpot.Arcolib
{
    /// <summary>
    /// An input source can be anything that provides a formatted amount of pixels data. 
    /// You can create your own input types and apply detection over static images, 
    /// web pages, videos a much more.
    /// </summary>
    public abstract class InputSource
    {
        /// <summary>
        /// Creates a new empty <see cref="InputSource"/> instance.
        /// </summary>
        /// <remarks>
        /// You should use this class as the base for all input sources inside the library. 
        /// </remarks>
        public InputSource()
        {
            _allocatedBytesCount = 0;
            _dataPtr = IntPtr.Zero;
        }


        /// <summary>
        /// External modules will call this function to grab the next frame from this source.
        /// </summary> 
        public abstract void GrabFrame();
      

        /// <summary>
        ///  Gets a texture representation of this source that is ready to be drawn into the screen. 
        /// </summary>
        /// <remarks>   
        ///  When you override this function, make sure that texture reference is persistent. 
        /// </remarks>   
        public abstract Texture texture {
            get;
        }

        /// <summary>
        /// For mobile platforms it will return the device rotation angle in degrees.
        /// </summary>
        /// <remarks>   
        /// This is useful to properly render the image on the device screen.
        /// </remarks>   
        public abstract int rotation  {
            get;
        }

        /// <summary>
        /// Returns true if the image is vertically flipped.
        /// </summary>
        public abstract bool verticallyMirrored
        {
            get;
        }

        /// <summary>
        /// Checks whether this source can be used by a tracker.
        /// </summary>      
        public abstract bool isReady {
            get;
        }

        /// <summary>
        /// Returns the current width of this source, measured in pixels.
        /// </summary>      
        public abstract int width
        {
            get;
        }

        /// <summary>
        /// Returns the current height of this source, measured in pixels.
        /// </summary>   
        public abstract int height
        {
            get;
        }

        /// <summary>
        /// Gets the amount of bytes needed for each pixel representation.
        /// </summary>
        /// <remarks>
        /// Primarily useful for parsing the data which contains the pixels information.
        /// </remarks>
        public abstract int bytesPerPixel
        {
            get;
        }

        /// <summary>
        /// Gets the internal pointer to the input pixels.
        /// </summary>
        /// <remarks>
        /// Useful to convert this input pixels to a <see cref="Texture2D"/> 
        /// or any other image representation.
        /// </remarks>
        public IntPtr pixelsData
        {
            get { return _dataPtr; }
        }

        /// <summary>
        /// Some input sources may need to be opened before being used.
        /// </summary>
        public abstract void Open();

        /// <summary>
        /// Some input sources may need to be closed explicitly.
        /// </summary>
        public abstract void Close();      

        /// <summary>
        /// Reserves memory for the internal pixel buffer when needed. 
        /// </summary>
        /// <param name="bytesCount">The current input buffer length.</param>
        protected void AllocateDataPtrIfNeeded(int bytesCount)
        {
            if(_dataPtr == IntPtr.Zero || _allocatedBytesCount != bytesCount)
            {
                if (_dataPtr != IntPtr.Zero)
                    Marshal.FreeHGlobal(_dataPtr);

                _dataPtr = Marshal.AllocHGlobal(bytesCount);
                _allocatedBytesCount = bytesCount;
            }
        }

        /// <summary>
        /// Pointer to the native object.
        /// </summary>
        internal  IntPtr _nativeInputSourcePtr = IntPtr.Zero;
        /// <summary>
        /// Pointer to the pixels buffer.
        /// </summary>
        protected IntPtr _dataPtr = IntPtr.Zero;
        /// <summary>
        /// The size of the current pixel buffer.
        /// </summary>
        protected int _allocatedBytesCount;       
    }
}
