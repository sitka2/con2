﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// Data container used to bind a <see cref="GameObject"/> to a certain
    /// barcode id.
    /// </summary>
    [System.Serializable]
    public class BarcodeBinding
    {
        /// <summary>
        /// The barcode id.
        /// </summary>
        public uint modelId;
        /// <summary>
        /// The user defined object to render when symbol is found.
        /// </summary>
        public GameObject gameobject = null;
        /// <summary>
        /// 
        /// </summary>
        public TrackerBehavior behaviour = null;
        /// <summary>
        /// 
        /// </summary>
        public bool useBehaviour = false;
    }

    /// <summary>
    /// This class allows the <see cref="BarcodeTracker"/> easy integration with Unity editor.  
    /// </summary>
    public class BarcodeTrackerInspector : MonoBehaviour, TrackerEventHandler<BarcodeSymbol>
    {
        /// <summary>
        /// The <see cref="InputSourceController"/> used for this tracker.
        /// </summary>
        public InputSourceController inputController;
        /// <summary>
        /// The amount symbols per frame used to initialize the <see cref="TrackerOptions"/>.
        /// </summary>
        public int maxSymbolsPerFrame = 3;
        /// <summary>
        /// Prints in the editor mode the id of <see cref="BarcodeSymbol"/>s found.
        /// </summary>
        public bool traceSymbols = false;
        /// <summary>
        /// Starts the tracker automatically.
        /// </summary>
        public bool autoStart = true;
        /// <summary>
        /// 
        /// </summary>
        public bool enableEvents = false;
        /// <summary>
        /// The list of bindings to render.
        /// </summary>
        public List<BarcodeBinding> bindingList = new List<BarcodeBinding>();
        /// <summary>
        /// 
        /// </summary>
        public BarcodeType barcodeType = BarcodeType.ArUco;
        /// <summary>
        /// 
        /// </summary>
        public TrackerEvent onSymbolFound = new TrackerEvent();
        /// <summary>
        /// 
        /// </summary>
        public TrackerEvent onSymbolUpdated = new TrackerEvent();
        /// <summary>
        /// 
        /// </summary>
        public TrackerEvent onSymbolLost = new TrackerEvent();
        /// <summary>
        /// 
        /// </summary>
        private BarcodeSymbolFrame _frame;
        /// <summary>
        /// 
        /// </summary>
        private TrackerOptions _options;
        /// <summary>
        /// 
        /// </summary>  
        private BarcodeTracker _tracker;
        /// <summary>
        /// 
        /// </summary>
        private TrackerEventDispatcher<BarcodeSymbol> _eventDispatcher;        

        /// <summary>
        /// 
        /// </summary>
        void Awake()
        {
            _eventDispatcher = new TrackerEventDispatcher<BarcodeSymbol>();
            _eventDispatcher.handler = this;

            if (inputController == null)
            {
                throw new InvalidOperationException("You must define the input controller for this tracker!");
            }

            inputController.onInputReady += OnInputControllerReady;           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="symbol"></param>
        public void OnSymbolFound(BarcodeSymbol symbol)
        {
            BarcodeBinding binding = bindingList.FirstOrDefault(s => s.modelId == symbol.id);

            if (binding != null && binding.behaviour != null)
                binding.behaviour.ApplyWhenSymbolFound(binding.gameobject, symbol);

            if (enableEvents)
                onSymbolFound.Invoke(symbol.id);           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="symbol"></param>
        public void OnSymbolUpdated(BarcodeSymbol symbol)
        {
            BarcodeBinding binding = bindingList.FirstOrDefault(s => s.modelId == symbol.id);

            if (binding != null && binding.behaviour != null)
                binding.behaviour.ApplyWhenSymbolUpdated(binding.gameobject, symbol);

            if (enableEvents)
                onSymbolUpdated.Invoke(symbol.id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="symbol"></param>
        public void OnSymbolLost(BarcodeSymbol symbol)
        {
            BarcodeBinding binding = bindingList.FirstOrDefault(s => s.modelId == symbol.id);

            if (binding != null && binding.behaviour != null)
                binding.behaviour.ApplyWhenSymbolLost(binding.gameobject, symbol);

            if(enableEvents)
              onSymbolLost.Invoke(symbol.id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="calibration"></param>
        private void OnInputControllerReady(InputSource input, DeviceCalibration calibration)
        {
            _options = new TrackerOptions(maxSymbolsPerFrame, 200);
            _frame = new BarcodeSymbolFrame();

            _tracker = new BarcodeTracker(calibration, _options);
            _tracker.input = input;
            _tracker.useBarcodeType = barcodeType;

            for (var i = 0; i < bindingList.Count; ++i)
            {
                BarcodeBinding binding = bindingList[i];

                if (binding.behaviour == null)
                    throw new InvalidOperationException("BarcodeBinding error on index " + i + ". You must define a behavior!");
            }

            if (autoStart)
                _tracker.Start();
        }

        /// <summary>
        /// Calls the internal tracker <see cref="BarcodeTracker.Start"/> function.
        /// </summary>
        public void StartTracker()
        {
            if (_tracker)
                _tracker.Start();
        }

        /// <summary>
        /// Calls the internal tracker <see cref="BarcodeTracker.Stop"/> function.
        /// </summary>
        public void StopTracker()
        {
            if (_tracker)
                _tracker.Stop();
        }

        /// <summary>
        /// Calls the internal tracker <see cref="BarcodeTracker.isRunning"/> function.
        /// </summary>  
        public bool IsTrackerRunning()
        {
            return _tracker && _tracker.isRunning;
        }

        /// <summary>
        /// Gets the internal tracker reference.
        /// </summary>      
        public BarcodeTracker GetTracker()
        {
            return _tracker;
        }

        /// <summary>
        /// Request and render the latest <see cref="BarcodeSymbolFrame"/> created by the internal tracker. 
        /// </summary>
        void Update()
        {
            if (_tracker)
            {
                _tracker.QueryFrame(ref _frame);
                _eventDispatcher.ComputeFrame(_frame);                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        void OnDestroy()
        {
            if (_tracker)
                _tracker.Stop();

            if (_eventDispatcher)
                _eventDispatcher.Reset();

            foreach (var binding in bindingList)
            {
                if (binding.behaviour != null)
                    binding.behaviour.Reset();
            }
            bindingList.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        void OnGUI()
        {
#if UNITY_EDITOR
            if (traceSymbols)
            {
                GUI.Label(new Rect(20, 20, 200, 40), "Detected barcodes: " + _frame.symbolCount.ToString());
                for (int j = 0; j < _frame.symbolCount; ++j)
                {
                   GUI.Label(new Rect(20, 40 + j * 20, 500, 40), "id:" + _frame[j].id.ToString() + " data: " + _frame[j].data);
                }
            }            
#endif
        }
    }
}
