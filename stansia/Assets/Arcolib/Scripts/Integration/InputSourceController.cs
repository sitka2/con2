﻿using UnityEngine;
using System.Collections;
using System;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// All implemented input sources types to be showed on editor.
    /// </summary>
    /// <remarks>
    /// Here you can define your customized input sources to be listed 
    /// inside the editor input source list. This is only if you need to show
    /// you input type in Editor mode.
    /// </remarks>
    public enum InputSourceType
    {
        /// <summary>
        /// Camera capture implementation based on Unity <see cref="WebCamTexture"/>.
        /// </summary>
        WebcamTextureInput,
        /// <summary>
        /// Native camera capture for each supported platforms.
        /// </summary>
        NativeCameraInput
    }

    /// <summary>
    ///  Video and image custom presets. Only used in editor mode.
    /// </summary>
    public enum VideoMode
    {
        /// <summary>
        /// User defined resolution and frame rate.
        /// </summary>
        CustomResolution,
        /// <summary>
        /// 640x480@30fps
        /// </summary>
        Resolution_640x480_30fps,
        /// <summary>
        /// 960x720@30fps
        /// </summary>
        Resolution_960x720_30fps,
        /// <summary>
        /// 1280x720@30fps
        /// </summary>
        Resolution_1280x720_30fps,
        /// <summary>
        /// 1920x1080@30fps
        /// </summary>
        Resolution_1920x1080_30fps
    }

    /// <summary>
    ///  Delegate used to notify when the selected input is ready to be used.
    /// </summary>
    /// <param name="input"></param>
    /// <param name="calibration"></param>
    public delegate void OnInputReady(InputSource input, DeviceCalibration calibration);

    /// <summary>
    ///  This class allows to provide the same input source
    ///  to multiple trackers in the scene (to achieve more efficiency).
    /// </summary>
    [DisallowMultipleComponent]
    [System.Serializable]
    public class InputSourceController : MonoBehaviour
    {
        /// <summary>
        /// The current device position.
        /// </summary>
        public CameraPosition devicePosition = CameraPosition.BackFace;
        /// <summary>
        /// The current device index.
        /// </summary>
        public int deviceIndex = 0;
        /// <summary>
        /// The <see cref="InputSource"/> width.
        /// </summary>
        public int inputWidth = 640;
        /// <summary>
        /// The <see cref="InputSource"/> height.
        /// </summary>
        public int inputHeight = 480;
        /// <summary>
        /// The capture frame rate.
        /// </summary>
        public int frameRate = 30;
        /// <summary>
        /// The video mode.
        /// </summary>
        public VideoMode videoMode = VideoMode.Resolution_640x480_30fps;
        /// <summary>
        /// The input type.
        /// </summary>
        public InputSourceType inputType = InputSourceType.WebcamTextureInput;
        /// <summary>
        /// Delegate called when selected input is ready.
        /// </summary>
        public event OnInputReady onInputReady;
        /// <summary>
        /// 
        /// </summary>
        public bool autoStart = true;
        /// <summary>
        /// 
        /// </summary>
        public bool enableAutoOrientation = true;
        /// <summary>
        /// 
        /// </summary>
        private InputSource _input = null;
        /// <summary>
        /// 
        /// </summary>
        private GameObject _screen;
        /// <summary>
        /// 
        /// </summary>
        private Camera _perspectiveCamera;
        /// <summary>
        /// 
        /// </summary>
        private Camera _orthographicCamera;
        /// <summary>
        /// 
        /// </summary> 
        private Transform _screenTransform;
        /// <summary>
        /// 
        /// </summary>
        private DeviceCalibration _calibration;

      
        /// <summary>
        /// 
        /// </summary>
        public void Open()
        {
            if (_input == null)
            {        
#if UNITY_EDITOR || UNITY_STANDALONE
                CreateInputSource((CameraPosition)deviceIndex, inputWidth, inputHeight, frameRate);
#else
                CreateInputSource(devicePosition, inputWidth, inputHeight, frameRate);
#endif
               StartCoroutine(SetupCameraRig());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Close()
        {
            if (_input != null)
            {
                _input.Close();
                _input = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public InputSource currentInput
        {
            get { return _input; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DeviceCalibration currentCalibration
        {
            get { return _calibration; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="devicePosition"></param>
        /// <param name="requestedWidth"></param>
        /// <param name="requestedHeight"></param>
        /// <param name="requestedFps"></param>
        void CreateInputSource(CameraPosition devicePosition, int requestedWidth, int requestedHeight, int requestedFps)
        {
            switch (inputType)
            {
                case InputSourceType.WebcamTextureInput:
                    _input = new WebcamTextureInput(devicePosition, inputWidth, inputHeight, frameRate);
                    break;

                case InputSourceType.NativeCameraInput:
                    _input = new NativeCameraInput(devicePosition, (uint)inputWidth, (uint)inputHeight);
                    break;

                default:
                    _input = new WebcamTextureInput(devicePosition, inputWidth, inputHeight, frameRate);
                    inputType = InputSourceType.WebcamTextureInput;
                    break;
            }

            _input.Open();
        }

        /// <summary>
        /// 
        /// </summary>
        void Update()
        {
            if (_input != null)
            {
                _input.GrabFrame();

                if (enableAutoOrientation)
                {
                    int rot = _input.rotation;
                    if (_orthographicCamera)                   
                        _orthographicCamera.transform.localEulerAngles = new Vector3(0, 0, rot);                  

                    if (_perspectiveCamera)                   
                        _perspectiveCamera.transform.localEulerAngles = new Vector3(0, 0, rot);                   
                }

                if (_screenTransform)
                {
                    int orientedHeight = !_input.verticallyMirrored ? _input.height : -(_input.height);
                    _screenTransform.localScale = new Vector3(_input.width, orientedHeight, 1);
                }
            }
        }


        /// <summary>
        /// Prints help and tracker results.
        /// </summary>
        void OnGUI()
        {
            // 8 - Prints result on screen.
            int screenW = Screen.width;
            int screenH = Screen.height;

            if(_input != null)
            GUI.Label(new Rect(20,20,screenW, screenH), _input.rotation.ToString());
           
        }


        /// <summary>
        /// 
        /// </summary>
        void Start()
        {
            if (autoStart)
                Open();
        }

        /// <summary>
        /// 
        /// </summary>
        void OnDestroy()
        {
            Close();      
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerator SetupCameraRig()
        {
            while (_input == null || _input.isReady == false)
            {
              yield return 0;
            }

            if (inputWidth != _input.width || inputHeight != _input.height)
            {
                inputWidth = _input.width;
                inputHeight = _input.height;
                videoMode = VideoMode.CustomResolution;
            }

            _calibration = new DeviceCalibration(inputWidth, inputHeight);

            var perspectiveCameraTransform = transform.Find("PerspectiveCamera");
            if (perspectiveCameraTransform)
            {
                ResetTransformToDefault(perspectiveCameraTransform);
                _perspectiveCamera = perspectiveCameraTransform.gameObject.GetComponent<Camera>();

                if (_perspectiveCamera)
                    _perspectiveCamera.fieldOfView = _calibration.verticalFieldOfView;
            }

            var orthographicCameraTransform = transform.Find("OrthographicCamera");
            if (orthographicCameraTransform)
            {
                _orthographicCamera = orthographicCameraTransform.gameObject.GetComponent<Camera>();

                if (_orthographicCamera)
                    _orthographicCamera.orthographicSize = _input.height / 2;
            }

            _screenTransform = transform.Find("Screen");
            if (_screenTransform)
            {
                _screenTransform.localEulerAngles = new Vector3(0, 0, 0);                
                _screen = _screenTransform.gameObject;

                if (_screen)
                {
                    MeshRenderer meshRenderer = _screen.GetComponent<MeshRenderer>();
                    meshRenderer.material.mainTexture = _input.texture;
                }
            }

             if (onInputReady != null)
                onInputReady(_input, _calibration);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="t"></param>
        private void ResetTransformToDefault(Transform t)
        {
            t.position = Vector3.zero;
            t.rotation = Quaternion.identity;
            t.localScale = new Vector3(1, 1, 1);
        }
    }
}