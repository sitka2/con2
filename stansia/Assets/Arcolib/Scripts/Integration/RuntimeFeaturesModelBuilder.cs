﻿using UnityEngine;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// This demo shows how to create and register a <see cref="FeaturesModel"/> during runtime.
    /// You can grab the concept from this example and adapt it to other projects when
    /// runtime models are needed.
    /// </summary>
    public class RuntimeFeaturesModelBuilder : MonoBehaviour
    {
        /// <summary>
        /// The <see cref="FeaturesTracker"/> control panel.
        /// </summary>
        public FeaturesTrackerInspector featuresInspector;

        /// <summary>
        /// 
        /// </summary>
        public TrackerBehavior behaviour;

        /// <summary>
        /// 
        /// </summary>
        public GameObject target;

        /// <summary>
        /// This will create a dynamic <see cref="FeaturesModel"/> from
        /// the <see cref="InputSource"/> attached to the tracker.
        /// </summary>
        /// <remarks>
        /// You can also create the model from a region of the input source.
        /// <see cref="FeaturesModel.Create(Texture2D, Rect)"/>
        /// </remarks>
        public void BuildModelFromInput()
        {
            // You must attach the tracker panel to this component.
            if (!featuresInspector) return;

            // Gets the tracker instance from the inspector panel.
            FeaturesTracker tracker = featuresInspector.GetTracker();

            // Checks if input is good to be used.
            if (!tracker || !tracker.hasEnoughFeatures) return;

            // Gets the current input instance from the tracker.
            InputSource input = tracker.input;

            // Converts the input pixels data to Texture2D.
            Texture2D texture = new Texture2D(input.width, input.height, (TextureFormat)input.bytesPerPixel, false);
            int size = input.width * input.height * input.bytesPerPixel;
            texture.LoadRawTextureData(input.pixelsData, size);
            texture.Apply(false);

            // Creates the model from texture.
            FeaturesModel modelBuilder = new FeaturesModel();
            modelBuilder.Create(texture);

#if UNITY_EDITOR
            Debug.Log("Model Quality:" + modelBuilder.qualityLevel);
#endif

            // Create the binding only if quality level is acceptable.
            if (modelBuilder.qualityLevel >= 0.1)
            {
                var cgo = Instantiate(target);
                cgo.SetActive(false);
                AppendModelWithBinding(modelBuilder, cgo);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="customObject"></param>
        public void AppendModelWithBinding(FeaturesModel model, GameObject customObject)
        {
            // You must attach the tracker panel to this component.
            if (!featuresInspector) return;

            // Gets the tracker instance from the inspector panel.
            FeaturesTracker tracker = featuresInspector.GetTracker();

            if (tracker)
            {
                var binding = new FeaturesBinding() { gameobject = customObject, modelData = null, behaviour = this.behaviour };
                featuresInspector.bindingList.Add(binding);
                tracker.AppendTrainingData(model);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void RemoveAllBindings()
        {
            // You must attach the tracker panel to this component.
            if (!featuresInspector) return;

            // Gets the tracker instance from the inspector panel.
            FeaturesTracker tracker = featuresInspector.GetTracker();

            // Reset the tracker
            tracker.Reset();

            // Reset the behaviour
            behaviour.Reset();

            // Clear any existing binding.
            featuresInspector.bindingList.Clear();
        }
    }

}