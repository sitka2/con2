﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// Provides native access to device camera. Extends the <see cref="InputSource"/>.
    /// </summary>
    public sealed class NativeCameraInput : InputSource
    {
        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="devicePosition"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public NativeCameraInput(CameraPosition devicePosition, uint width, uint height) : base()
        {
            _devicePosition = devicePosition;

            _width = width;
            _height = height;

            _cameraCapture = new CameraCapture();
            _cameraCapture.pixelsFormat = PixelsFormat.RGB24;

            _texture = new Texture2D(1, 1, TextureFormat.RGB24, false);
            _texture.LoadRawTextureData(new byte[] { 0x0,0x0,0x0 });
            _texture.Apply(false); 

            _nativeInputSourcePtr = NativeCameraInputCreateReference(_cameraCapture._nativePtr);           
        }

        /// <summary>
        /// Release the unused native memory
        /// </summary>
        ~NativeCameraInput()
        {
           NativeCameraInputDisposeReference(_nativeInputSourcePtr);

            if (_dataPtr != IntPtr.Zero)
                Marshal.FreeHGlobal(_dataPtr);
        }

        /// <inheritdoc/>
        public override void GrabFrame()
        {
            int width = 0, height = 0, bpp = 0;
     
            if (NativeCameraInputGrabFrame(_nativeInputSourcePtr, ref width, ref height, ref bpp))
            {
                AllocateDataPtrIfNeeded(width * height * bpp);
                if (_texture == null || _texture.width != width || _texture.height != height) {
                    _texture.Resize(width, height);              
                }

                NativeCameraInputReadFrameToTexture2D(_nativeInputSourcePtr, _dataPtr);
                _texture.LoadRawTextureData(_dataPtr, _allocatedBytesCount);
                _texture.Apply(false);
            }
        }

        /// <inheritdoc/>
        public override void Open()
        {
            if(_cameraCapture != null)
            _cameraCapture.Open((uint)_devicePosition, _width,_height);
        }

        /// <inheritdoc/>
        public override void Close()
        {
            if (_cameraCapture != null)
                _cameraCapture.Close();
        }

        /// <inheritdoc/>
        public override bool isReady
        {
            get { return NativeCameraInputIsReady(_nativeInputSourcePtr); }
        }

        /// <inheritdoc/>
        public override int width
        {
            get { return NativeCameraInputGetWidth(_nativeInputSourcePtr); }
        }

        /// <inheritdoc/>
        public override int height
        {
            get { return NativeCameraInputGetHeight(_nativeInputSourcePtr); }
        }

        /// <inheritdoc/>
        public override int bytesPerPixel
        {
            get { return NativeCameraInputGetBytesPerPixel(_nativeInputSourcePtr); }
        }

        /// <inheritdoc/>
        public override Texture texture
        {
            get { return _texture; }        
        }

        /// <summary>
        /// 
        /// </summary>
        public override int rotation
        {
            get { return NativeCameraInputRotation(_nativeInputSourcePtr); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool verticallyMirrored
        {
            get  { return false; }
        }

        /// <summary>
        /// 
        /// </summary>
        private Texture2D _texture;

        /// <summary>
        /// 
        /// </summary>
        private CameraCapture _cameraCapture;

        /// <summary>
        /// 
        /// </summary>
        private CameraPosition _devicePosition;

        /// <summary>
        /// 
        /// </summary>
        private uint _width;

        /// <summary>
        /// 
        /// </summary>
        private uint _height;

        [DllImport(Config.libraryPath, EntryPoint = "NativeCameraInputCreateReference", CallingConvention = Config.callConvention)]
        private static extern IntPtr NativeCameraInputCreateReference(IntPtr cameraCapture);

        [DllImport(Config.libraryPath, EntryPoint = "NativeCameraInputGrabFrame", CallingConvention = Config.callConvention)]
        private static extern bool NativeCameraInputGrabFrame(IntPtr ptr, ref int width, ref int height, ref int bpp);

        [DllImport(Config.libraryPath, EntryPoint = "NativeCameraInputReadFrameToTexture2D", CallingConvention = Config.callConvention)]
        private static extern void NativeCameraInputReadFrameToTexture2D(IntPtr ptr, IntPtr pixels);

        [DllImport(Config.libraryPath, EntryPoint = "NativeCameraInputDisposeReference", CallingConvention = Config.callConvention)]
        private static extern void NativeCameraInputDisposeReference(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "NativeCameraInputIsReady", CallingConvention = Config.callConvention)]
        private static extern bool NativeCameraInputIsReady(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "NativeCameraInputGetWidth", CallingConvention = Config.callConvention)]
        private static extern int NativeCameraInputGetWidth(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "NativeCameraInputGetHeight", CallingConvention = Config.callConvention)]
        private static extern int NativeCameraInputGetHeight(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "NativeCameraInputGetBytesPerPixel", CallingConvention = Config.callConvention)]
        private static extern int NativeCameraInputGetBytesPerPixel(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "NativeCameraInputRotation", CallingConvention = Config.callConvention)]
        private static extern int NativeCameraInputRotation(IntPtr ptr);
       
    }
}
