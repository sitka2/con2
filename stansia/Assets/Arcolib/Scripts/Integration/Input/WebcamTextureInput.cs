﻿using System;
using UnityEngine;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// Unity's <see cref="WebCamTexture"/> based input.
    /// </summary>
    public class WebcamTextureInput : ImageInput
    {
        /// <summary>
        /// 
        /// </summary>
        static protected WebCamTexture _webcamTexture;
        /// <summary>
        /// 
        /// </summary>
        static protected WebCamDevice _cameraDevice;

        /// <summary>
        /// 
        /// </summary>
        protected Color32[] _colorBuffer;
        /// <summary>
        /// 
        /// </summary>
        protected byte[] _pixelBuffer;
        /// <summary>
        /// 
        /// </summary>
        protected int _requestedWidth, _requestedHeight, _requestedFps;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="devicePosition"></param>
        /// <param name="requestedWidth"></param>
        /// <param name="requestedHeight"></param>
        /// <param name="requestedFps"></param>
        public WebcamTextureInput(CameraPosition devicePosition, int requestedWidth, int requestedHeight, int requestedFps) : base()
        {
            _requestedWidth = requestedWidth;
            _requestedHeight = requestedHeight;
            _requestedFps = requestedFps;
            InitCamera((int)devicePosition);
        }
        
        /// <summary>
        /// 
        /// </summary>
        public override Texture texture
        {
            get { return _webcamTexture; }
        }

        /// <summary>
        /// For mobile platforms it will return the device rotation angle in degrees.
        /// </summary>
        public override int rotation
        {
            get { return _webcamTexture.videoRotationAngle; }
        }


        /// <summary>
        /// Returns true if the image is vertically flipped.
        /// </summary>
        public override bool verticallyMirrored
        {
            get { return _webcamTexture.videoVerticallyMirrored; }
        }

        /// <inheritdoc/>
        public override bool isReady
        {
            get { return base.isReady && _webcamTexture.width > 16 && _webcamTexture.height > 16; }
        }

        /// <inheritdoc/>
        public override void Open()
        {
            if (_webcamTexture && !_webcamTexture.isPlaying)
            { 
                _webcamTexture.Play();
            }
        }

        /// <inheritdoc/>
        public override void Close()
        {
            if (_webcamTexture)
            {
                _webcamTexture.Stop();
                _webcamTexture = null;
            }
        }

        /// <inheritdoc/>
        public override void GrabFrame()
        {
            if (_webcamTexture && _webcamTexture.didUpdateThisFrame)
            {
                ResizeBufferIfNeeded();
                ImageFlipMode flipMode = ImageFlipMode.Vertical;                     
                ImageUtils.WebCamTextureToData(_webcamTexture, _pixelBuffer, flipMode,  _colorBuffer);
                Apply(_pixelBuffer, _webcamTexture.width, _webcamTexture.height, 4);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceIndex"></param>
        private void InitCamera(int deviceIndex)
        {
            if (WebCamTexture.devices.Length > 0)
            {
                if (_webcamTexture == null)
                {
                    _cameraDevice = WebCamTexture.devices[deviceIndex];
                    _webcamTexture = new WebCamTexture(_cameraDevice.name, _requestedWidth, _requestedHeight, _requestedFps);
                    _webcamTexture.mipMapBias = -0.5f;
                    _webcamTexture.anisoLevel = 1;                   
                } 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void ResizeBufferIfNeeded()
        {
            int w = _webcamTexture.width;
            int h = _webcamTexture.height;
            int pixelsLength = w * h;

            if (_pixelBuffer == null || _colorBuffer == null || _colorBuffer.Length != pixelsLength)
            {
                _colorBuffer = new Color32[w * h];
                _pixelBuffer = new byte[w * h * 4];              
            }
        }
    }
}