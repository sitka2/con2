﻿using System;
using UnityEngine;
using Vexpot.Arcolib.Integration;

namespace Vexpot.Arcolib.Samples
{
    /// <summary>
    /// Customized input source based on local image file.
    /// </summary>
    /// <remarks>
    /// Inheriting the <see cref="ImageInput"/> class you can create your own input types.
    /// </remarks>
    public class Texture2DInput : ImageInput
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public Texture2DInput(Texture2D texture) : base()
        {
            if (texture.format == TextureFormat.RGB24 || texture.format == TextureFormat.RGBA32)
            {
                _texture = texture;
                Apply(texture.GetRawTextureData(), texture.width, texture.height, (int)texture.format);
            }
            else throw new InvalidOperationException("TextureFormat is not allowed!");
        }

        /// <inheritdoc/>
        public override void GrabFrame()
        {

        }

        /// <inheritdoc/>
        public override void Open()
        {

        }

        /// <inheritdoc/>
        public override void Close()
        {

        }

        /// <inheritdoc/>
        public override int rotation
        {
            get { return 0; }
        }

        /// <inheritdoc/>
        public override bool verticallyMirrored
        {
            get { return false; }
        }

        /// <inheritdoc/> 
        public override Texture texture
        {
            get { return _texture; }
        }

        /// <summary>
        /// Reference to the image.
        /// </summary>
        private Texture2D _texture;

    }
}
