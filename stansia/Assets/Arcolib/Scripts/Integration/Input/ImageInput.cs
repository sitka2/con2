﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// Base class for all image based <see cref="InputSource"/>s.
    /// </summary>
    public abstract class ImageInput : InputSource
    {
        /// <summary>
        /// Creates a new empty <see cref="ImageInput"/> instance.
        /// </summary>
        /// <remarks>
        /// You should use this class as the base for all image sources inside the library. 
        /// </remarks>
        public ImageInput() : base()
        {
            _nativeInputSourcePtr = BridgeInputSourceCreateReference();        
        }

        /// <summary>
        /// Release the unused native memory
        /// </summary>
        ~ImageInput()
        {
            try
            {
                BridgeInputSourceDisposeReference(_nativeInputSourcePtr);

                if (_dataPtr != IntPtr.Zero)
                    Marshal.FreeHGlobal(_dataPtr);
            }
            catch
            {
                throw new InvalidOperationException("ImageInput.DisposeNativeReferences has failed!");
            }
        }

        /// <summary>
        /// Uploads the data to the native input source and creates a 
        /// new data frame to be computed.
        /// </summary>
        /// <param name="data">The image pixels.</param>
        /// <param name="width">The image current width.</param>
        /// <param name="height">The image current height.</param>
        /// <param name="bytesPerPixel">The amount of bytes needed for each pixel representation.</param>
        protected virtual void Apply(byte[] data, int width, int height, int bytesPerPixel)
        {
            if (data != null)
            {
                try
                {
                    AllocateDataPtrIfNeeded(data.Length);
                    Marshal.Copy(data, 0, _dataPtr, data.Length);
                    BridgeInputSourceApply(_nativeInputSourcePtr, _dataPtr, width, height, bytesPerPixel);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Checks whether this source can be used by a tracker.
        /// </summary>
        /// <remarks> 
        /// When you call <see cref="ImageInput.Apply"/> with valid parameters
        /// this property will be set to true.
        /// </remarks> 
        public override bool isReady
        {
            get { return BridgeInputSourceIsReady(_nativeInputSourcePtr); }
        }

        /// <summary>
        /// Returns the current width of this source, measured in pixels.
        /// </summary>
        /// <remarks> 
        /// If you never call <see cref="ImageInput.Apply"/> the value will be 0.
        /// </remarks> 
        public override int width
        {
            get { return BridgeInputSourceGetWidth(_nativeInputSourcePtr); }
        }

        /// <summary>
        /// Returns the current height of this source, measured in pixels.
        /// </summary>
        /// <remarks> 
        /// If you never call <see cref="ImageInput.Apply"/> the value will be 0.
        /// </remarks> 
        public override int height
        {
            get { return BridgeInputSourceGetHeight(_nativeInputSourcePtr); }
        }

        /// <summary>
        /// Gets the amount of bytes needed for each pixel representation.
        /// </summary>
        /// <remarks>
        /// Primarily useful for parsing the data which contains the pixels information.
        /// </remarks>
        public override int bytesPerPixel
        {
            get { return BridgeInputSourceGetBytesPerPixel(_nativeInputSourcePtr); }
        }

        [DllImport(Config.libraryPath, EntryPoint = "BridgeInputSourceCreateReference", CallingConvention = Config.callConvention)]
        private static extern IntPtr BridgeInputSourceCreateReference();

        [DllImport(Config.libraryPath, EntryPoint = "BridgeInputSourceDisposeReference", CallingConvention = Config.callConvention)]
        private static extern void BridgeInputSourceDisposeReference(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "BridgeInputSourceIsReady", CallingConvention = Config.callConvention)]
        private static extern bool BridgeInputSourceIsReady(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "BridgeInputSourceGetWidth", CallingConvention = Config.callConvention)]
        private static extern int BridgeInputSourceGetWidth(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "BridgeInputSourceGetHeight", CallingConvention = Config.callConvention)]
        private static extern int BridgeInputSourceGetHeight(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "BridgeInputSourceGetBytesPerPixel", CallingConvention = Config.callConvention)]
        private static extern int BridgeInputSourceGetBytesPerPixel(IntPtr ptr);

        [DllImport(Config.libraryPath, EntryPoint = "BridgeInputSourceApply", CallingConvention = Config.callConvention)]
        private static extern void BridgeInputSourceApply(IntPtr ptr, IntPtr data, int width, int height, int bytesPerPixel);

    }
}
