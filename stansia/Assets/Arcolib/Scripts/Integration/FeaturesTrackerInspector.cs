﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// Data container used to bind a <see cref="GameObject"/> to a certain
    /// features model.
    /// </summary>
    [System.Serializable]
    public class FeaturesBinding
    {
        /// <summary>
        /// The binary data to create the model.
        /// </summary>
        public TextAsset modelData = null;
        /// <summary>
        /// The user defined object with 3D objects to render when symbol is found.
        /// </summary>
        public GameObject gameobject = null;
        /// <summary>
        /// 
        /// </summary>
        public TrackerBehavior behaviour;
        /// <summary>
        /// 
        /// </summary>
        public bool useBehaviour = false;
        /// <summary>
        /// 
        /// </summary>
        public bool enabled = true;
    }    

    /// <summary>
    /// This class allows the <see cref="FeaturesTracker"/> easy integration with Unity editor. 
    /// </summary>
    public class FeaturesTrackerInspector : MonoBehaviour, TrackerEventHandler<FeaturesSymbol>
    {
        /// <summary>
        /// The <see cref="InputSourceController"/> used for this tracker.
        /// </summary>
        public InputSourceController inputController;
        /// <summary>
        /// The amount symbols per frame used to initialize the <see cref="TrackerOptions"/>.
        /// </summary>
        public int maxSymbolsPerFrame = 3;
        /// <summary>
        /// When this is true the tracking is turned off.
        /// </summary>
        public bool detectOnly = false;
        /// <summary>
        /// 
        /// </summary>
        public bool traceSymbols = false;
        /// <summary>
        /// Starts the tracker automatically.
        /// </summary>
        public bool autoStart = true;
        /// <summary>
        /// 
        /// </summary>
        public bool enableEvents = false;
        /// <summary>
        /// The list of bindings to render.
        /// </summary>
        public List<FeaturesBinding> bindingList = new List<FeaturesBinding>();
        /// <summary>
        /// 
        /// </summary>
        public TrackerEvent onSymbolFound = new TrackerEvent();
        /// <summary>
        /// 
        /// </summary>
        public TrackerEvent onSymbolUpdated = new TrackerEvent();
        /// <summary>
        /// 
        /// </summary>
        public TrackerEvent onSymbolLost = new TrackerEvent();
        /// <summary>
        /// 
        /// </summary>
        private FeaturesSymbolFrame _frame;
        /// <summary>
        /// 
        /// </summary>
        private TrackerOptions _options;
        /// <summary>
        /// 
        /// </summary>
        private FeaturesTracker _tracker;
        /// <summary>
        /// 
        /// </summary>
        private TrackerEventDispatcher<FeaturesSymbol> _eventDispatcher;

        /// <summary>
        /// 
        /// </summary>
        void Awake()
        {
            _eventDispatcher = new TrackerEventDispatcher<FeaturesSymbol>();
            _eventDispatcher.handler = this;

            if (inputController == null)
            {
                throw new InvalidOperationException("You must define the input controller for this tracker!");
            }

            inputController.onInputReady += OnInputControllerReady;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="calibration"></param>
        private void OnInputControllerReady(InputSource input, DeviceCalibration calibration)
        {
            _options = new TrackerOptions(maxSymbolsPerFrame, 200);
            _frame = new FeaturesSymbolFrame();

            _tracker = new FeaturesTracker(calibration, _options);
            _tracker.input = input;
            _tracker.detectOnly = detectOnly;

            for (var i = 0; i < bindingList.Count; ++i)
            {
                FeaturesBinding binding = bindingList[i];

                if (binding.modelData == null)
                    throw new InvalidOperationException("FeaturesBinding error on index " + i + ". You must set the model data!");

                FeaturesModel model = new FeaturesModel();
                model.Read(binding.modelData);              
                model.enabled = binding.enabled;
                _tracker.AppendTrainingData(model);
            }

            if (autoStart)
                _tracker.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="symbol"></param>
        public void OnSymbolFound(FeaturesSymbol symbol)
        {
            if (symbol.id < bindingList.Count)
            {
                FeaturesBinding binding = bindingList[(int)symbol.id];

                if (binding.behaviour != null)
                    binding.behaviour.ApplyWhenSymbolFound(binding.gameobject, symbol);
            }

            if (enableEvents)
                onSymbolFound.Invoke(symbol.id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="symbol"></param>
        public void OnSymbolUpdated(FeaturesSymbol symbol)
        {
            if (symbol.id < bindingList.Count)
            {
                FeaturesBinding binding = bindingList[(int)symbol.id];

                if (binding.behaviour != null)
                    binding.behaviour.ApplyWhenSymbolUpdated(binding.gameobject, symbol);
            }

            if (enableEvents)
                onSymbolUpdated.Invoke(symbol.id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="symbol"></param>
        public void OnSymbolLost(FeaturesSymbol symbol)
        {
            if (symbol.id < bindingList.Count)
            {
                FeaturesBinding binding = bindingList[(int)symbol.id];

                if (binding.behaviour != null)
                    binding.behaviour.ApplyWhenSymbolLost(binding.gameobject, symbol);
            }

            if (enableEvents)
                onSymbolLost.Invoke(symbol.id);
        }

        /// <summary>
        /// Calls the internal tracker <see cref="FeaturesTracker.Start"/> function.
        /// </summary>
        public void StartTracker()
        {
            if (_tracker)
                _tracker.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        public void EnableModelAt(int index, bool value)
        {
            if (_tracker)
            {
                var model = _tracker.GetTrainingDataByIndex(index);

                if(model != null)
                {
                    model.enabled = value;
                }
            }
        }

        /// <summary>
        /// Calls the internal tracker <see cref="FeaturesTracker.isRunning"/> function.
        /// </summary>     
        public bool IsTrackerRunning()
        {
            return _tracker && _tracker.isRunning;
        }

        /// <summary>
        /// Gets the internal tracker reference.
        /// </summary>      
        public FeaturesTracker GetTracker()
        {
            return _tracker;
        }

        /// <summary>
        /// Calls the internal tracker <see cref="FeaturesTracker.Stop"/> function.
        /// </summary>
        public void StopTracker()
        {
            if (_tracker)
                _tracker.Stop();
        }
        
        /// <summary>
        /// Request and render the latest <see cref="FeaturesSymbolFrame"/> created by the internal tracker. 
        /// </summary>
        void Update()
        {
            if (_tracker)
            {
                _tracker.QueryFrame(ref _frame);
                _eventDispatcher.ComputeFrame(_frame);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        void OnDestroy()
        {
            if (_tracker)
            {
                _tracker.Stop();
                _tracker.Reset();
            }

            if (_eventDispatcher)
                _eventDispatcher.Reset();

            foreach (var binding in bindingList)
            {
                if (binding.behaviour != null)
                    binding.behaviour.Reset();
            }
            bindingList.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        void OnGUI()
        {
#if UNITY_EDITOR
            if (traceSymbols)
            {
                GUI.Label(new Rect(20, 20, 200, 40), "Detected symbols: " + _frame.symbolCount.ToString());
                for (int j = 0; j < _frame.symbolCount; ++j)
                {
                    GUI.Label(new Rect(20, 40 + j * 20, 200, 40), "id:" + _frame[j].id.ToString());
                }
            }
#endif
        }
    }
}