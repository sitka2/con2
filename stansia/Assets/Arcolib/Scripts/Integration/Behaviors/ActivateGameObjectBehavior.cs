﻿using System.Collections.Generic;
using UnityEngine;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// Default behavior to activate or deactivate gameobjects when symbols are detected.
    /// </summary>
    [CreateAssetMenu(fileName = "ActivateGameObjectBehavior", menuName = "Arcolib/Tracker Behaviors/Activate GameObject", order = 1)]
    [System.Serializable]
    public class ActivateGameObjectBehavior : TrackerBehavior
    {
        /// <summary>
        /// 
        /// </summary>
        public ActivateGameObjectBehavior()
        {
            _gameObjectsCache = new Dictionary<uint, GameObject>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameobject">The GameObject containing components needed for the action.</param>
        /// <param name="symbol">The symbol involved in the action.</param>
        public override void ApplyWhenSymbolFound(GameObject gameobject, ISymbol symbol)
        {
            if (!_gameObjectsCache.ContainsKey(symbol.id))
            {
                if (gameobject)
                {
                    if (gameobject.scene.name != null)
                        _gameObjectsCache[symbol.id] = gameobject;
                    else
                    {
                        var temp = GameObject.Instantiate(gameobject);
                        temp.SetActive(false);
                        _gameObjectsCache[symbol.id] = temp;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameobject">The GameObject containing components needed for the action.</param>
        /// <param name="symbol">The symbol involved in the action.</param>
        public override void ApplyWhenSymbolUpdated(GameObject gameobject, ISymbol symbol)
        {
            if (_gameObjectsCache.ContainsKey(symbol.id))
            {
                GameObject objectRef = _gameObjectsCache[symbol.id];

                if (objectRef)
                {
                    if (!objectRef.activeInHierarchy)
                        objectRef.SetActive(true);

                    symbol.transformMatrix.CopyTo(objectRef.transform);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameobject">The GameObject containing components needed for the action.</param>
        /// <param name="symbol">The symbol involved in the action.</param>
        public override void ApplyWhenSymbolLost(GameObject gameobject, ISymbol symbol)
        {
            if (_gameObjectsCache.ContainsKey(symbol.id))
            {
                GameObject objectRef = _gameObjectsCache[symbol.id];
                objectRef.SetActive(false);
            }
        }

        /// <inheritdoc/>
        public override void Reset()
        {
            foreach (var obj in _gameObjectsCache)
            {
                Destroy(obj.Value);
            }

            _gameObjectsCache.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<uint, GameObject> _gameObjectsCache;
    }
}
