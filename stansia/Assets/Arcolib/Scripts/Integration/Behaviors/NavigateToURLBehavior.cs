﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// Default behavior to navigate to certain URL when symbols are detected.
    /// </summary>
    [System.Serializable]
    public class URLSymbolData
    {
        /// <summary>
        /// 
        /// </summary>
        public string url;
        /// <summary>
        /// 
        /// </summary>
        public uint id;
    }
    /// <summary>
    /// 
    /// </summary>
    [CreateAssetMenu(fileName = "NavigateToURLBehavior", menuName = "Arcolib/Tracker Behaviors/Navigate To URL", order = 1)]
    [System.Serializable]
    public class NavigateToURLBehavior : TrackerBehavior
    {
        /// <summary>
        /// 
        /// </summary>
        public List<URLSymbolData> URLMap;       

        /// <summary>
        /// 
        /// </summary>
        public NavigateToURLBehavior()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameobject">The GameObject containing components needed for the action.</param>
        /// <param name="symbol">The symbol involved in the action.</param>
        public override void ApplyWhenSymbolFound(GameObject gameobject, ISymbol symbol)
        {
            URLSymbolData data = URLMap.FirstOrDefault(s => s.id == symbol.id);

            if (data != null)
                Application.OpenURL(data.url);
        }   
    }      
}
