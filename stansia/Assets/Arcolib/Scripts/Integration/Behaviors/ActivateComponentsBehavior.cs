﻿using System.Collections.Generic;
using UnityEngine;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// Default behavior to activate or deactivate components when symbols are detected.
    /// </summary>
    [CreateAssetMenu(fileName = "ActivateComponentsBehavior", menuName = "Arcolib/Tracker Behaviors/Activate Components", order = 1)]
    [System.Serializable]
    public class ActivateComponentsBehavior : TrackerBehavior
    {
        /// <summary>
        /// 
        /// </summary>
        protected class ComponentsGroup
        {
            private Renderer[] renderers;
            private Collider[] colliders;

            /// <summary>
            /// 
            /// </summary>
            /// <param name="value"></param>
            public void SetEnable(bool value)
            {
                if (renderers != null)
                {
                    foreach (var renderer in renderers)
                        renderer.enabled = value;
                }

                if (colliders != null)
                {
                    foreach (var collider in colliders)
                        collider.enabled = value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="gameobject"></param>
            public void RetrieveComponentsIn(GameObject gameobject)
            {
                this.renderers = gameobject.GetComponentsInChildren<Renderer>();
                this.colliders = gameobject.GetComponentsInChildren<Collider>();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ActivateComponentsBehavior()
        {
            _componentsCache = new Dictionary<uint, ComponentsGroup>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameobject">The GameObject containing components needed for the action.</param>
        /// <param name="symbol">The symbol involved in the action.</param>
        public override void ApplyWhenSymbolFound(GameObject gameobject, ISymbol symbol)
        {
            if (!_componentsCache.ContainsKey(symbol.id))
            {
                if (gameobject)
                {
                    var components = new ComponentsGroup();
                    if (gameobject.scene.name != null)
                    {
                        components.RetrieveComponentsIn(gameobject);                        
                    }
                    else
                    {
                        var temp = GameObject.Instantiate(gameobject);
                        components.RetrieveComponentsIn(temp);
                    }
                    _componentsCache[symbol.id] = components;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameobject">The GameObject containing components needed for the action.</param>
        /// <param name="symbol">The symbol involved in the action.</param>
        public override void ApplyWhenSymbolUpdated(GameObject gameobject, ISymbol symbol)
        {
            if (_componentsCache.ContainsKey(symbol.id))
            {
                var components = _componentsCache[symbol.id];

                if (components != null)
                    components.SetEnable(true);

                if (gameobject)
                    symbol.transformMatrix.CopyTo(gameobject.transform);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameobject">The GameObject containing components needed for the action.</param>
        /// <param name="symbol">The symbol involved in the action.</param>
        public override void ApplyWhenSymbolLost(GameObject gameobject, ISymbol symbol)
        {
            if (_componentsCache.ContainsKey(symbol.id))
            {
                var components = _componentsCache[symbol.id];
                if (components != null)
                    components.SetEnable(false);
            }
        }

        /// <inheritdoc/>
        public override void Reset()
        {
            _componentsCache.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<uint, ComponentsGroup> _componentsCache;
    }
}
