﻿using UnityEngine;

namespace Vexpot.Arcolib.Integration
{
    /// <summary>
    /// Base class for all tracker behaviors. 
    /// </summary>
    /// <remarks>
    /// Behaviors describes one or multiple actions to be performed when symbols are found.
    /// The actions can be separated in three phases: <see cref="ApplyWhenSymbolFound"/>,
    /// <see cref="ApplyWhenSymbolUpdated"/> or 
    /// <see cref="ApplyWhenSymbolLost"/>
    /// </remarks>
    [System.Serializable]
    public abstract class TrackerBehavior : ScriptableObject
    {
        /// <summary>
        /// Implements the actions taken when symbols are detected for first time.
        /// </summary>
        /// <param name="gameobject">The GameObject containing components needed for the action.</param>
        /// <param name="symbol">The symbol involved in the action.</param>
        public virtual void ApplyWhenSymbolFound(GameObject gameobject, ISymbol symbol)
        {
            // you will need to override this function in subclasses.            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameobject">The GameObject containing components needed for the action.</param>
        /// <param name="symbol">The symbol involved in the action.</param>
        public virtual void ApplyWhenSymbolUpdated(GameObject gameobject, ISymbol symbol)
        {
            // you will need to override this function in subclasses.
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameobject">The GameObject containing components needed for the action.</param>
        /// <param name="symbol">The symbol involved in the action.</param>
        public virtual void ApplyWhenSymbolLost(GameObject gameobject, ISymbol symbol)
        {
            // you will need to override this function in subclasses.
        }
        /// <summary>
        /// Resets this behavior to its initial state.
        /// </summary>
        public virtual void Reset()
        {
            // you will need to override this function in subclasses.
        }
    }
}
