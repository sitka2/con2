﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Vexpot.Arcolib.Integration;
using UnityEditorInternal;
using UnityEditor.Events;

namespace Vexpot.Arcolib.CustomEditors
{
    /// <summary>
    /// BarcodeTrackerInspector custom inspector
    /// </summary>
    [CustomEditor(typeof(FaceTrackerInspector))]
    public class FaceTrackerEditor : UnityEditor.Editor
    {
        /// <summary>
        /// 
        /// </summary>
        private FaceTrackerInspector _content;
        /// <summary>
        /// 
        /// </summary>
        private List<FaceBinding> _bindingList;
        /// <summary>
        /// 
        /// </summary>
        private ReorderableList _bindingListRenderer;
        /// <summary>
        /// 
        /// </summary>
        private SerializedProperty _onSymbolFoundEvent;
        /// <summary>
        /// 
        /// </summary>
        private SerializedProperty _onSymbolLostEvent;
        /// <summary>
        /// 
        /// </summary>
        private SerializedProperty _onSymbolUpdatedEvent;
        /// <summary>
        /// 
        /// </summary>
        private EventPhase _eventPhaseToListen = EventPhase.Found | EventPhase.Lost;

        /// <summary>
        /// 
        /// </summary>
        void OnEnable()
        {
            _bindingListRenderer = new ReorderableList(serializedObject, serializedObject.FindProperty("bindingList"),
                  false, true, true, true);

            _bindingListRenderer.drawHeaderCallback = ListHeaderCallbackDelegate;
            _bindingListRenderer.drawElementCallback = ListDrawElementDelegate;
            _bindingListRenderer.elementHeightCallback = ElementHeightCallbackDelegate;

            _onSymbolFoundEvent = serializedObject.FindProperty("onSymbolFound");
            _onSymbolUpdatedEvent = serializedObject.FindProperty("onSymbolUpdated");
            _onSymbolLostEvent = serializedObject.FindProperty("onSymbolLost");            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        private void ListHeaderCallbackDelegate(Rect rect)
        {
            EditorGUI.LabelField(rect, "Currently Using (" + _bindingList.Count + ") Models");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="index"></param>
        /// <param name="isActive"></param>
        /// <param name="isFocused"></param>
        private void ListDrawElementDelegate(Rect rect, int index, bool isActive, bool isFocused)
        {
            FaceBinding bindingReference = _bindingList[index];
            rect.height = EditorGUIUtility.singleLineHeight;

            Rect originalRect = rect;

            rect.width = originalRect.width * 0.4f; 
            EditorGUI.LabelField(rect, EditorTitlesAndHints.faceIdInfo);
            rect.x += rect.width;
            rect.width = originalRect.width * 0.60f;
            bindingReference.faceId = (uint)EditorGUI.LongField(rect, bindingReference.faceId);
            
            rect.y += 20;
            rect.x = originalRect.x + 10;
            bindingReference.useBehaviour = EditorGUI.Foldout(rect, bindingReference.useBehaviour, EditorTitlesAndHints.behaviorSettingsInfo, true);

            if (bindingReference.useBehaviour)
            {
                rect.width = originalRect.width * 0.98f;
                rect.y += 20;
                bindingReference.behaviour = (TrackerBehavior)EditorGUI.ObjectField(rect, EditorTitlesAndHints.behaviorInfo, bindingReference.behaviour, typeof(TrackerBehavior), false);
                rect.y += 20;
                bindingReference.gameobject = (GameObject)EditorGUI.ObjectField(rect, EditorTitlesAndHints.gameobjectTitleAndHint, bindingReference.gameobject, typeof(GameObject), true);
            }

            rect.width += 20;
            rect.y += EditorGUIUtility.singleLineHeight * 1.4f;
            Handles.color = DrawUtils.separaratorColor;
            Handles.DrawLine(new Vector3(30, rect.y), new Vector3(rect.width, rect.y));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private float ElementHeightCallbackDelegate(int index)
        {
            return _bindingList[index].useBehaviour ? 90 : 50;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnInspectorGUI()
        {
            _content = (FaceTrackerInspector)target;
            ArcolibIconManager.ApplyIconToObject(ArcolibIconManager.faceTrackerIcon, _content);
            _bindingList = _content.bindingList;   
    
            EditorGUILayout.Separator();
            EditorGUI.BeginChangeCheck();           

            _content.inputController = (InputSourceController)EditorGUILayout.ObjectField(EditorTitlesAndHints.inputControllerInfo, _content.inputController, typeof(InputSourceController), true);
            _content.faceTrainingData = (TextAsset)EditorGUILayout.ObjectField(EditorTitlesAndHints.faceDataInfo, _content.faceTrainingData, typeof(TextAsset), false);
            EditorGUI.BeginDisabledGroup(Application.isPlaying);
            _content.maxSymbolsPerFrame = EditorGUILayout.IntSlider(EditorTitlesAndHints.symbolPerFrameInfo, _content.maxSymbolsPerFrame,1,10);
            EditorGUI.EndDisabledGroup();
            _content.traceSymbols = EditorGUILayout.Toggle(EditorTitlesAndHints.traceSymbolsInfo, _content.traceSymbols);
            _content.autoStart = EditorGUILayout.Toggle(EditorTitlesAndHints.autoStarInfo, _content.autoStart);
            _content.enableEvents = EditorGUILayout.Toggle(EditorTitlesAndHints.enableEventsInfo, _content.enableEvents);

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Tracker Controls");
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal();

            EditorGUI.BeginDisabledGroup(!Application.isPlaying || _bindingList.Count == 0);
            //GUI.enabled = !_content.IsTrackerRunning();
            if (GUILayout.Button("Start", GUILayout.Height(30)))
            {
                _content.StartTracker();
            }

            //GUI.enabled = _content.IsTrackerRunning();
            if (GUILayout.Button("Stop", GUILayout.Height(30)))
            {
                _content.StopTracker();
            }
            EditorGUI.EndDisabledGroup();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            EditorGUILayout.Separator();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);  /// Binding list
            EditorGUILayout.LabelField("Binding List");
            EditorGUILayout.Separator();

            serializedObject.Update();
            if (_bindingListRenderer != null)
            {
                _bindingListRenderer.DoLayoutList();
            }
            serializedObject.ApplyModifiedProperties();

            if (_bindingList.Count > 0)
                if (GUILayout.Button(EditorTitlesAndHints.removeBindingsInfo, GUILayout.Height(30)))
                {
                    if (EditorUtility.DisplayDialog("Remove Bindings", "Are you sure you want to remove all bindings?\nYou cannot undo this action.", "Yes", "No"))
                        _bindingList.Clear();
                }

            EditorGUILayout.EndVertical();  // End Binding list
            EditorGUILayout.Separator();

            if (_content.enableEvents)
            {
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);  // events
                _eventPhaseToListen = (EventPhase)EditorGUILayout.EnumMaskField("Event Phase", _eventPhaseToListen);

                EditorGUILayout.Separator();

                if ((_eventPhaseToListen & EventPhase.Found) == EventPhase.Found)
                    EditorGUILayout.PropertyField(_onSymbolFoundEvent);

                if ((_eventPhaseToListen & EventPhase.Updated) == EventPhase.Updated)
                    EditorGUILayout.PropertyField(_onSymbolUpdatedEvent);

                if ((_eventPhaseToListen & EventPhase.Lost) == EventPhase.Lost)
                    EditorGUILayout.PropertyField(_onSymbolLostEvent);

                serializedObject.ApplyModifiedProperties();

                int foundEventCount = _content.onSymbolFound.GetPersistentEventCount();
                int updatedEventCount = _content.onSymbolUpdated.GetPersistentEventCount();
                int lostEventCount = _content.onSymbolLost.GetPersistentEventCount();

                EditorGUI.BeginDisabledGroup(foundEventCount == 0 && lostEventCount == 0 && updatedEventCount == 0);
                if (GUILayout.Button(EditorTitlesAndHints.removeEventsInfo, GUILayout.Height(30)))
                {
                    if (EditorUtility.DisplayDialog("Remove Events", "Are you sure you want to remove all events?\nYou cannot undo this action.", "Yes", "No"))
                    {
                        for (int i = 0; i < foundEventCount; i++)
                            UnityEventTools.RemovePersistentListener(_content.onSymbolFound, 0);

                        for (int i = 0; i < updatedEventCount; i++)
                            UnityEventTools.RemovePersistentListener(_content.onSymbolUpdated, 0);

                        for (int j = 0; j < lostEventCount; j++)
                            UnityEventTools.RemovePersistentListener(_content.onSymbolLost, 0);
                    }
                }
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.EndVertical();  // End events 
            }

            if (EditorGUI.EndChangeCheck())
                EditorUtility.SetDirty(target);

            EditorGUILayout.Separator();

            if (_bindingList.Count == 0)
            {
                EditorGUILayout.HelpBox("You must add at least one binding!", MessageType.Warning);
            }

            if (_eventPhaseToListen == 0 && _content.enableEvents)
            {
                EditorGUILayout.HelpBox("You have enabled events listening but no phase is being used!", MessageType.Warning);
            }
        }
    }
}
