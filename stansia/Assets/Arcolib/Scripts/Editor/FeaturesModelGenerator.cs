﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
using System.IO;
using System.Collections;
using System;

namespace Vexpot.Arcolib.CustomEditors
{
    /// <summary>
    /// 
    /// </summary>
    public class FeaturesModelGenerator : IModelGenerator
    {
        [System.Serializable]
        private class FeaturesModelEditorData
        {
            public string fileName = "Browse image file...";
            public string imageUrl = string.Empty;
            public bool invalidated = false;
            public float quality = 0;
            public string modified = "Never";
            public Texture2D preview = null;
            /// <summary>
            /// 
            /// </summary>
            public FeaturesModelEditorData()
            {

            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="url"></param>
            public void ResolveUrlAndName(string url)
            {
                imageUrl = url;

                if (String.IsNullOrEmpty(url))
                {
                    fileName = "Browse image file...";
                }
                else {
                    fileName = Path.GetFileName(imageUrl);
                    modified = DateTime.Now.ToShortTimeString();
                }

                invalidated = true;
                quality = 0;
                UnityEngine.Object.DestroyImmediate(preview);
                preview = null;
            }
        }

       // static private EditorWindow _instance;

        private string _latestImageUrl;
        private float _progress = 0.0f;
        private int _selectedItem = -1;
        private int _listItemPadding = 5;
        private int _modelsProcessed;
        private int _modelsSucceededCount;
        private int _modelsFailedCount;
        private int _modelsSkippedCount;
        private string _destinationFilePath;
        private IEnumerator _modelCreationRoutine;
        private Rect _reusableRectangle = new Rect();
        private Rect _previewRectangle = new Rect();
        private ReorderableList _modelDataListRenderer;
        private Vector2 _scrollPosition = new Vector2();
        private List<FeaturesModelEditorData> _modelDataList = new List<FeaturesModelEditorData>();
        private readonly float _defaultListItemHeight = EditorGUIUtility.singleLineHeight * 2.8f;
        private EditorWindow _parentWindow;

        /// <summary>
        /// 
        /// </summary>
        public void OnEnable(EditorWindow parentWindow)
        {
            _parentWindow = parentWindow;
            _latestImageUrl = Application.dataPath;
            _modelDataListRenderer = new ReorderableList(_modelDataList, typeof(FeaturesModelEditorData),
                  false, true, true, true);

            _modelDataListRenderer.drawHeaderCallback = ListHeaderCallbackDelegate;
            _modelDataListRenderer.drawElementCallback = ListDrawElementDelegate;
            _modelDataListRenderer.onSelectCallback = ListSelectCallbackDelegate;
            _modelDataListRenderer.elementHeightCallback = ListElementHeightCallbackDelegate;
            _modelDataListRenderer.onChangedCallback = ListChangedCallbackDelegate;
        }

        /// <summary>
        /// 
        /// </summary>
        public void OnGUI()
        {
            DrawGeneratorToolHeader();
            EditorGUILayout.Separator();
            DrawAddFromDirectoryButton();
            EditorGUILayout.Separator();
            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);
            DrawModelList();
            EditorGUILayout.Separator();
            EditorGUILayout.EndScrollView();
            DrawMessages();
            DrawControlButtons();
        }

        /// <summary>
        /// 
        /// </summary>
        public void DrawMessages()
        {
            //if (_modelDataList.Count == 0)
            //    EditorGUILayout.HelpBox("Add some images to the generator.", MessageType.Info);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        public void OnPreviewTexture(Rect rect)
        {
            if (_selectedItem != -1 && _selectedItem < _modelDataList.Count)
            {
                var currentModel = _modelDataList[_selectedItem];
                if (currentModel.preview)
                {
                    _previewRectangle.Set(0, 0, currentModel.preview.width, currentModel.preview.height);
                    DrawUtils.ComputeRectWithAspectRatio(_previewRectangle, rect, ref _reusableRectangle);
                    EditorGUI.DrawPreviewTexture(_reusableRectangle, currentModel.preview);
                }
                else
                {
                    DrawNoPreviewAvailable(rect);
                }
            }
            else
            {
                DrawNoPreviewAvailable(rect);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        private void DrawNoPreviewAvailable(Rect rect)
        {
            rect.x = rect.width * 0.5f - 50;
            rect.y = rect.height * 0.5f + 15;
            EditorGUI.LabelField(rect, "No Preview Available", EditorStyles.whiteLabel);
        }

        /// <summary>
        /// 
        /// </summary>
        public void OnDestroy()
        {
            DisposeResourceCache();
        }

        /// <summary>
        /// 
        /// </summary>
        private void DrawAddFromDirectoryButton()
        {
            if (GUILayout.Button("Add images from directory"))
            {
                string folderName = EditorUtility.OpenFolderPanel("Choose the images", "", "");

                try
                {
                    var files = Directory.GetFiles(folderName);

                    foreach (var image in files)
                    {
                        string lowerCasePath = image.ToLower();
                        if (Path.GetExtension(lowerCasePath) == ".jpg" ||
                        Path.GetExtension(lowerCasePath) == ".png" ||
                        Path.GetExtension(lowerCasePath) == ".jpeg")
                        {
                            if (File.ReadAllBytes(image).Length > 0)
                            {
                                var modelData = new FeaturesModelEditorData();
                                modelData.ResolveUrlAndName(image);
                                _modelDataList.Add(modelData);
                            }
                        }
                    }

                    if (_modelDataList.Count > 0)
                        _selectedItem = _modelDataList.Count - 1;
                }
                catch (Exception e)
                {
                    _parentWindow.ShowNotification(new GUIContent(e.Message));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void DrawModelList()
        {
            if (_modelDataListRenderer != null)
            {
                _modelDataListRenderer.DoLayoutList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void DrawGeneratorToolHeader()
        {
            EditorGUILayout.LabelField("Features Model Generator", EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Add one or multiple images to be encoded as features models. Features models contains a set of descriptors and keypoints used to recognize and track images during runtime.", EditorStyles.wordWrappedLabel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        private void ListHeaderCallbackDelegate(Rect rect)
        {
            EditorGUI.LabelField(rect, "Images (" + _modelDataList.Count + ")", EditorStyles.boldLabel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        private void ListSelectCallbackDelegate(ReorderableList list)
        {
            _selectedItem = _modelDataListRenderer.index;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        private void ListChangedCallbackDelegate(ReorderableList list)
        {
            if (_modelDataList.Count > 0)
                _selectedItem = _modelDataList.Count - 1;
            else
                _selectedItem = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        void OnLostFocus()
        {
            _selectedItem = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private float ListElementHeightCallbackDelegate(int index)
        {
            return _defaultListItemHeight + _listItemPadding;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="index"></param>
        /// <param name="isActive"></param>
        /// <param name="isFocused"></param>
        private void ListDrawElementDelegate(Rect rect, int index, bool isActive, bool isFocused)
        {
            FeaturesModelEditorData currentModel = _modelDataList[index];
            currentModel.invalidated = false;

            float originalWidth = rect.width;
            float originalX = rect.x;

            rect.width = originalWidth * 0.80f;
            EditorGUI.LabelField(rect, currentModel.fileName);

            rect.x = rect.width + 5;
            rect.y += 2;
            rect.width = originalWidth * 0.20f;
            rect.height = EditorGUIUtility.singleLineHeight;

            if (GUI.Button(rect, "Browse"))
            {
                var filters = new string[] { "Image files", "png,jpg,jpeg" };
                _latestImageUrl = EditorUtility.OpenFilePanelWithFilters("Open image file", _latestImageUrl, filters);
                currentModel.ResolveUrlAndName(_latestImageUrl);

                if (!string.IsNullOrEmpty(currentModel.fileName))
                {
                    _selectedItem = index;
                }
            }

            if (currentModel.preview == null || currentModel.invalidated) {
                if (File.Exists(currentModel.imageUrl)) {
                    currentModel.preview = ImageUtils.LoadImage(currentModel.imageUrl, ImageFlipMode.Vertical);
                }
            }

            rect.width = originalWidth * 0.55f;
            rect.x = originalX;
            rect.y += EditorGUIUtility.singleLineHeight * 1.2f;

            DrawUtils.DrawRating(rect, "Quality", currentModel.quality, ArcolibIconManager.starIcon, ArcolibIconManager.starDeactivatedIcon);
            rect.x = rect.width;
            rect.width = originalWidth * 0.45f;
            EditorGUI.LabelField(rect, "Last modified " + currentModel.modified);

            rect.y += EditorGUIUtility.singleLineHeight * 1.3f;
            Handles.color = DrawUtils.separaratorColor;
            Handles.DrawLine(new Vector3(10, rect.y), new Vector3(originalWidth, rect.y));
        }

        /// <summary>
        /// 
        /// </summary>
        private void DrawControlButtons()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(_modelDataList.Count <= 0);
            if (GUILayout.Button("Build Models", GUILayout.Height(30)))
            {
                CreateFeaturesModelsAsync();
            }

            if (GUILayout.Button("Remove All Images", GUILayout.Height(30)))
            {
                if (EditorUtility.DisplayDialog("Remove Images", "Are you sure you want to remove all images?\nYou cannot undo this action.", "Yes", "No"))
                {
                    DisposeResourceCache();
                }
            }

            EditorGUI.EndDisabledGroup();
            EditorGUILayout.EndHorizontal();
        }

        private void DisposeResourceCache()
        {
            _selectedItem = -1;

            if (_modelDataList != null)
                _modelDataList.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateHandler()
        {
            if (!_modelCreationRoutine.MoveNext())
            {
                EditorApplication.update -= UpdateHandler;
            }

            _progress = _modelsProcessed / (float)_modelDataList.Count;
            EditorUtility.DisplayProgressBar("Hold on please", "Creating features models...", _progress);

            if (_progress >= 1)
                EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateFeaturesModelsAsync()
        {
            _progress = 0;
            _modelCreationRoutine = CreateFeaturesModels();
            EditorApplication.update += UpdateHandler;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerator CreateFeaturesModels()
        {
            _modelsSucceededCount = 0;
            _modelsFailedCount = 0;
            _modelsSkippedCount = 0;


            _destinationFilePath = Application.dataPath + "/Arcolib/Resources";

            if (!Directory.Exists(_destinationFilePath))
                Directory.CreateDirectory(_destinationFilePath);

            _modelsProcessed = 0;
            FeaturesModel modelFactory = new FeaturesModel();
            while (_modelsProcessed < _modelDataList.Count)
            {
                FeaturesModelEditorData generatorModel = _modelDataList[_modelsProcessed];

                if (generatorModel.quality == 0)
                    modelFactory.PlotKeyPoints(generatorModel.preview);

                if (File.Exists(generatorModel.imageUrl))
                {
                    modelFactory.Create(generatorModel.imageUrl);
                    generatorModel.quality = modelFactory.qualityLevel;
                    generatorModel.modified = DateTime.Now.ToShortTimeString();

                    string modelFileName = _destinationFilePath + "/" + Path.GetFileNameWithoutExtension(generatorModel.fileName) + ".bytes";

                    if (modelFactory.Save(modelFileName))
                    {
                        _modelsSucceededCount++;
                    }
                    else
                    {
                        _modelsFailedCount++;
                        generatorModel.quality = 0;
                    }
                }
                else {
                    _modelsSkippedCount++;
                }
                _modelsProcessed++;
                yield return null;
            }

            AssetDatabase.Refresh();
            EditorUtility.DisplayDialog("Build info", GetBuildStats(), "Got it!");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetBuildStats()
        {
            string buildResults = "\n\nBuild: " + _modelsSucceededCount + " succeeded, " + _modelsFailedCount + " failed, " + _modelsSkippedCount + " skipped.";
            string message = _modelsSucceededCount > 0 ? "Features models were created in: " + _destinationFilePath : "No features models were built.";
            return message + buildResults;
        }
    }
}