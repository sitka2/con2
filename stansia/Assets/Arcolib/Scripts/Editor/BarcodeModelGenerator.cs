﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Vexpot.Arcolib.CustomEditors
{
    public interface IModelGenerator
    {
        void OnPreviewTexture(Rect rect);
        void OnGUI();
    }

    public class BarcodeModelGenerator : IModelGenerator
    {
        /// <summary>
        /// 
        /// </summary>
        [Flags]
        private enum ModelErrorCodes
        {
            None,
            InvalidRange = 2,
            DuplicatedValue = 4,
            EmptyGenerationDir = 8,
            InvalidDataSize = 16,
        }

        /// <summary>
        /// 
        /// </summary>
        private enum GenerationMode
        {
            Manual,
            Automatic
        }

        /// <summary>
        /// 
        /// </summary>
        private enum ModelImageResolution
        {
            Resolution128x128 = 128,
            Resolution256x256 = 256,
            Resolution512x512 = 512
        }

        [System.Serializable]
        private class BarcodeModelEditorData
        {
            public string data = "";
            public uint id;
            public Texture2D preview = null;
        }

        private int _endId = 1;
        private int _startId = 0;
        private int _modelsProcessed;
        private float _progress = 0.0f;
        private bool _withColors = true;
        private string _generationDir = string.Empty;
        private List<BarcodeModelEditorData> _modelList = new List<BarcodeModelEditorData>();
        private Vector2 _scrollPosition = new Vector2();
        private GenerationMode _generationMode = GenerationMode.Manual;
        private int _barcodeType = 0;
        private static GUIContent[] _barcodeTypeOptions = new GUIContent[] { new GUIContent("ArUco"), new GUIContent("QrCode") };
        private ModelImageResolution _generationResolution = ModelImageResolution.Resolution256x256;
        private ReorderableList _modelDataListRenderer;
        private IEnumerator _modelCreationRoutine;
        private EditorWindow _parentWindow;
        private ModelErrorCodes _errorCode;
        private Rect _reusableRectangle = new Rect();
        private Rect _previewRectangle = new Rect();
        private int _selectedItem = -1;


        public void OnEnable(EditorWindow parentWindow)
        {
            _parentWindow = parentWindow;
            _modelDataListRenderer = new ReorderableList(_modelList, typeof(int),
                  false, true, true, true);

            _modelDataListRenderer.drawHeaderCallback = ListHeaderCallbackDelegate;
            _modelDataListRenderer.drawElementCallback = ListDrawElementDelegate;
            _modelDataListRenderer.elementHeightCallback = ListElementHeightCallbackDelegate;
            _modelDataListRenderer.onSelectCallback = ListSelectCallbackDelegate;
            _modelDataListRenderer.onChangedCallback = ListChangedCallbackDelegate;
        }

        /// <summary>
        /// 
        /// </summary>
        public void OnDestroy()
        {
            _modelList.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        public void OnGUI()
        {
            DrawGeneratorToolHeader();
            EditorGUILayout.Separator();
            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);
            DrawGenerationOptions();

            switch (_generationMode)
            {
                case GenerationMode.Manual:
                    DrawManualMode();
                    break;
                case GenerationMode.Automatic:
                    DrawAutoMode();
                    break;
            }

            EditorGUILayout.EndScrollView();
            _errorCode = CheckForModelsError();
            DrawMessages();
            DrawControlButtons(_errorCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        public void OnPreviewTexture(Rect rect)
        {
            if (_selectedItem != -1 && _selectedItem < _modelList.Count)
            {
                var currentModel = _modelList[_selectedItem];
                if (currentModel.preview)
                {
                    _previewRectangle.Set(0, 0, currentModel.preview.width, currentModel.preview.height);
                    DrawUtils.ComputeRectWithAspectRatio(_previewRectangle, rect, ref _reusableRectangle);
                    EditorGUI.DrawPreviewTexture(_reusableRectangle, currentModel.preview);
                }
                else
                {
                    DrawNoPreviewAvailable(rect);
                }
            }
            else {
                DrawNoPreviewAvailable(rect);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        private void DrawNoPreviewAvailable(Rect rect)
        {
            rect.x = rect.width * 0.5f - 50;
            rect.y = rect.height * 0.5f + 15;
            EditorGUI.LabelField(rect, "No Preview Available", EditorStyles.whiteLabel);
        }

        /// <summary>
        /// 
        /// </summary>
        public void DrawMessages()
        {
            if ((_errorCode & ModelErrorCodes.InvalidRange) == ModelErrorCodes.InvalidRange)
                EditorGUILayout.HelpBox("Data values must be between 0 to 1023!", MessageType.Error);

            if ((_errorCode & ModelErrorCodes.InvalidDataSize) == ModelErrorCodes.InvalidDataSize)
                EditorGUILayout.HelpBox("Data to be encoded as QR can only contain 1024 characters!", MessageType.Error);

            if ((_errorCode & ModelErrorCodes.DuplicatedValue) == ModelErrorCodes.DuplicatedValue)
                EditorGUILayout.HelpBox("Some data values are duplicated!", MessageType.Error);

            if ((_errorCode & ModelErrorCodes.EmptyGenerationDir) == ModelErrorCodes.EmptyGenerationDir)
                EditorGUILayout.HelpBox("You must choose a location to store your generated models!", MessageType.Error);

            //if (_generationMode == GenerationMode.Manual && _modelList.Count == 0)
            //    EditorGUILayout.HelpBox("Add some model ID!", MessageType.Info);
        }

        /// <summary>
        /// 
        /// </summary>
        private void DrawGeneratorToolHeader()
        {
            EditorGUILayout.LabelField("Barcode Model Generator", EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Creates generic barcode models. Models generated are fiducials patterns commonly known as markers.", EditorStyles.wordWrappedLabel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private float ListElementHeightCallbackDelegate(int index)
        {
            return 30;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        private void ListSelectCallbackDelegate(ReorderableList list)
        {
            _selectedItem = _modelDataListRenderer.index;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        private void ListChangedCallbackDelegate(ReorderableList list)
        {
            if (_modelList.Count > 0)
                _selectedItem = _modelList.Count - 1;
            else
                _selectedItem = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        private void DrawGenerationOptions()
        {
            EditorGUILayout.LabelField("Directory", EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Choose the location where you want to store generated files.", EditorStyles.wordWrappedLabel);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(_generationDir, EditorStyles.wordWrappedLabel);

            if (GUILayout.Button("Browse", GUILayout.Width(60)))
            {
                _generationDir = EditorUtility.OpenFolderPanel("Choose directory", _generationDir, "");

                if (!Directory.Exists(_generationDir))
                {
                    _parentWindow.ShowNotification(new GUIContent("Invalid directory path"));
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.LabelField("Generation Options", EditorStyles.boldLabel);

            EditorGUI.BeginChangeCheck();

            _barcodeType = EditorGUILayout.Popup(EditorTitlesAndHints.barcodeTypeInfo, _barcodeType, _barcodeTypeOptions);

            bool generationModeChanged = EditorGUI.EndChangeCheck();

            _generationResolution = (ModelImageResolution)EditorGUILayout.EnumPopup(EditorTitlesAndHints.approxResolutionInfo, _generationResolution, GUILayout.Height(18));

            if (_barcodeType == 0)
            {
                _withColors = EditorGUILayout.Toggle(EditorTitlesAndHints.buildWithColorsInfo, _withColors);
                _generationMode = (GenerationMode)EditorGUILayout.EnumPopup("Choose Generation Mode", _generationMode, GUILayout.Height(20));
            }
            else
                _generationMode = GenerationMode.Manual;           

            if (generationModeChanged)
                _modelList.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorCode"></param>
        private void DrawControlButtons(ModelErrorCodes errorCode)
        {
            EditorGUI.BeginDisabledGroup((_generationMode == 0 && _modelList.Count <= 0) || errorCode != ModelErrorCodes.None);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Build Models", GUILayout.Height(30)))
            {
                CreateBarcodeModelsAsync();
            }
            EditorGUI.EndDisabledGroup();

            if (_generationMode == GenerationMode.Manual)
            {
                EditorGUI.BeginDisabledGroup(_modelList.Count <= 0);
                if (GUILayout.Button("Remove All", GUILayout.Height(30)))
                {
                    if (EditorUtility.DisplayDialog("Remove Models", "Are you sure you want to remove all models?\nYou cannot undo this action.", "Yes", "No"))
                    {
                        _modelList.Clear();
                    }
                }

                EditorGUI.EndDisabledGroup();
            }

            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private ModelErrorCodes CheckForModelsError()
        {
            ModelErrorCodes error = ModelErrorCodes.None;
            bool invalidRange = false;
            bool duplicatedValue = false;
            bool invalidSize = false;

            if (_generationMode == GenerationMode.Manual)
            {
                if (_barcodeType == 1)
                {
                    duplicatedValue = _modelList.GroupBy(x => x.data).Any(g => g.Count() > 1);
                    invalidSize = _modelList.GroupBy(x => x.data).Any(g => g.Key.Length > 1024);
                }
                else {
                    uint from = 1024, to = 0;
                    for (var r = 0; r < _modelList.Count; r++)
                    {
                        uint value = _modelList[r].id;
                        from = Math.Min(from, value);
                        to = Math.Max(to, value);
                    }

                    duplicatedValue = _modelList.GroupBy(x => x.id).Any(g => g.Count() > 1);
                    invalidRange = ((_modelList.Count > 0) ? (from < 0 || to > 1023 || from > to) : false);
                }
            }
            else
            {
                invalidRange = _startId < 0 || _endId > 1023 || _startId > _endId || _startId == _endId;
            }

            if (invalidSize)
                error |= ModelErrorCodes.InvalidDataSize;

            if (invalidRange)
                error |= ModelErrorCodes.InvalidRange;

            if (duplicatedValue)
                error |= ModelErrorCodes.DuplicatedValue;

            if (string.IsNullOrEmpty(_generationDir))
                error |= ModelErrorCodes.EmptyGenerationDir;

            return error;
        }

        /// <summary>
        /// 
        /// </summary>
        private void DrawManualMode()
        {
            if (_modelDataListRenderer != null)
                _modelDataListRenderer.DoLayoutList();

            EditorGUILayout.Separator();
        }

        /// <summary>
        /// 
        /// </summary>
        private void DrawAutoMode()
        {
            EditorGUILayout.LabelField("Model IDs Range", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Start ", GUILayout.Width(50));
            _startId = EditorGUILayout.IntField(_startId);
            EditorGUILayout.LabelField("End ", GUILayout.Width(50));
            _endId = EditorGUILayout.IntField(_endId);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Separator();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        private void ListHeaderCallbackDelegate(Rect rect)
        {
            EditorGUI.LabelField(rect, "Model IDs (" + _modelList.Count + ")", EditorStyles.boldLabel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="index"></param>
        /// <param name="isActive"></param>
        /// <param name="isFocused"></param>
        private void ListDrawElementDelegate(Rect rect, int index, bool isActive, bool isFocused)
        {
            if (_barcodeType == 1)
            {
                string value = _modelList[index].data;
                _modelList[index].data = EditorGUI.TextField(rect, "Barcode Data", value);
            }
            else
            {
               uint value = _modelList[index].id;
               float originalWidth = rect.width;
               rect.width = originalWidth * 0.4f;
               EditorGUI.LabelField(rect, "Barcode Data");
               rect.x += rect.width;
               rect.width = originalWidth * 0.6f;
               _modelList[index].id  = (uint)EditorGUI.LongField(rect,value);               
            }

            rect.y += EditorGUIUtility.singleLineHeight * 1.6f;
            Handles.color = DrawUtils.separaratorColor;
            Handles.DrawLine(new Vector3(10, rect.y), new Vector3(rect.width, rect.y));
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateBarcodeModelsAsync()
        {
            _progress = 0;
            _modelCreationRoutine = CreateBarcodeModels();
            EditorApplication.update += UpdateHandler;
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateHandler()
        {
            if (!_modelCreationRoutine.MoveNext())
            {
                EditorApplication.update -= UpdateHandler;
            }

            _progress = _modelsProcessed / (float)_modelList.Count;
            EditorUtility.DisplayProgressBar("Hold on please", "Creating barcode models...", _progress);

            if (_progress >= 1)
                EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// 
        /// </summary>
        private IEnumerator CreateBarcodeModels()
        {
            if (_generationMode == GenerationMode.Automatic)
            {
                _modelList.Clear();
                for (var r = _startId; r <= _endId; r++)
                {
                    var barcodeData = new BarcodeModelEditorData();
                    barcodeData.data = r.ToString();
                    _modelList.Add(barcodeData);
                }
            }

            _modelsProcessed = 0;

            BarcodeModel barcodeFactory = new BarcodeModel();
            for (var r = 0; r < _modelList.Count; r++)
            {
                uint idCode = 0;              

                if (_barcodeType == 0)
                {
                    idCode = _modelList[r].id;
                    barcodeFactory.Create(idCode, (int)_generationResolution, _withColors);
                }
                else {
                    string data = _modelList[r].data;
                    idCode = TrackerUtils.GetHashCode(data);
                    barcodeFactory.Create(data, (int)_generationResolution);
                }

                _modelList[r].preview = barcodeFactory.GetImage();

                string outputFile = Path.Combine(_generationDir, "Marker_" + idCode + ".jpg");
                barcodeFactory.Save(outputFile);
                _modelsProcessed++;
                yield return null;
            }

            EditorUtility.DisplayDialog("Build info", GetBuildStats(), "Got it!");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetBuildStats()
        {
            string buildResults = "\n\nBuild: " + _modelList.Count + " succeeded";
            string message = _modelList.Count > 0 ? "Barcode models were created in: " + _generationDir : "No barcode models were built.";
            return message + buildResults;
        }      

    }
}
