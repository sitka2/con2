﻿using UnityEditor;
using UnityEngine;
using Vexpot.Arcolib.Integration;

namespace Vexpot.Arcolib.CustomEditors
{
    /// <summary>
    /// InputSourceController custom inspector.
    /// </summary>
    [CustomEditor(typeof(InputSourceController))]
    public class InputSourceEditor : Editor
    {
        private InputSourceController _content;
        private GUIContent[] _deviceNames;
        private int _latestDeviceCount = -1;

        /// <summary>
        /// 
        /// </summary>
        public override void OnInspectorGUI()
        {
            _content = (InputSourceController)target;
            ArcolibIconManager.ApplyIconToObject(ArcolibIconManager.inputSourceIcon, _content);
          
            if (_latestDeviceCount != WebCamTexture.devices.Length)
            {
                _deviceNames = new GUIContent[WebCamTexture.devices.Length];
                _latestDeviceCount = 0;

                foreach (var dev in WebCamTexture.devices)
                {
                    GUIContent deviceInfo = new GUIContent();
                    deviceInfo.text = (_latestDeviceCount == 0) ? dev.name + " (Default)" : dev.name;
                    _deviceNames[_latestDeviceCount] = deviceInfo; 
                    _latestDeviceCount++;
                }
            }

            if (_latestDeviceCount == 1)
                _content.deviceIndex = 0;

            EditorGUILayout.Separator();

            EditorGUI.BeginChangeCheck();
            
            EditorGUILayout.BeginVertical();
            _content.inputType = (InputSourceType)EditorGUILayout.EnumPopup(EditorTitlesAndHints.inputSourceInfo, _content.inputType);
            _content.videoMode = (VideoMode)EditorGUILayout.EnumPopup(EditorTitlesAndHints.videoModeInfo, _content.videoMode);

            if (_content.videoMode == VideoMode.CustomResolution)
            {
                _content.inputWidth = EditorGUILayout.IntField("Image Width", _content.inputWidth);
                _content.inputHeight = EditorGUILayout.IntField("Image Height", _content.inputHeight);
                _content.frameRate = EditorGUILayout.IntField("Frame Rate (FPS)", _content.frameRate);
            }
            else SetupFromVideoMode(_content.videoMode);
            _content.autoStart = EditorGUILayout.Toggle(EditorTitlesAndHints.inputAutoStarInfo, _content.autoStart);
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Input Controls");
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal();

            GUI.enabled = _content.currentInput == null && Application.isPlaying;
            if (GUILayout.Button(EditorTitlesAndHints.inputOpenInfo, GUILayout.Height(30)))
            {
                _content.Open();
            }

            GUI.enabled = _content.currentInput != null;
            if (GUILayout.Button(EditorTitlesAndHints.inputCloseInfo, GUILayout.Height(30)))
            {
                _content.Close();
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            GUI.enabled = true;

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Editor & Standalone Options", EditorStyles.boldLabel);
            _content.deviceIndex = EditorGUILayout.Popup(EditorTitlesAndHints.cameraDeviceInfo, _content.deviceIndex, _deviceNames);
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Mobile Options", EditorStyles.boldLabel);
            _content.devicePosition = (CameraPosition)EditorGUILayout.EnumPopup(EditorTitlesAndHints.cameraPositionInfo, _content.devicePosition);
            _content.enableAutoOrientation = EditorGUILayout.Toggle(EditorTitlesAndHints.autoOrientationInfo, _content.enableAutoOrientation);
            EditorGUILayout.EndVertical();

            if (EditorGUI.EndChangeCheck())
                EditorUtility.SetDirty(target);

            if (_latestDeviceCount == 0)
            {
                EditorGUILayout.HelpBox("No camera device found!", MessageType.Error);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        private void SetupFromVideoMode(VideoMode mode)
        {
            switch (mode)
            {
                case VideoMode.Resolution_640x480_30fps:
                    _content.inputWidth = 640; _content.inputHeight = 480; _content.frameRate = 30;
                    break;
                case VideoMode.Resolution_960x720_30fps:
                    _content.inputWidth = 960; _content.inputHeight = 720; _content.frameRate = 30;
                    break;
                case VideoMode.Resolution_1280x720_30fps:
                    _content.inputWidth = 1280; _content.inputHeight = 720; _content.frameRate = 30;
                    break;
                case VideoMode.Resolution_1920x1080_30fps:
                    _content.inputWidth = 1920; _content.inputHeight = 1080; _content.frameRate = 30;
                    break;
                default:
                    break;
            }

        }
    }
}
