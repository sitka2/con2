﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Vexpot.Arcolib.CustomEditors
{
    /// <summary>
    /// Custom inspectors titles and hints
    /// </summary>
    static class EditorTitlesAndHints
    {
        static public GUIContent inputControllerInfo = new GUIContent("Input Controller", "The input used by this tracker.");
        static public GUIContent autoStarInfo = new GUIContent("Auto Start", "Starts tracking automatically when the component is started.");
        static public GUIContent symbolPerFrameInfo = new GUIContent("Symbols Per Frame", "The amount of symbols allowed to be detected per frame.");
        static public GUIContent idInfo = new GUIContent("Id", "The unique id for the barcode symbol.");
        static public GUIContent faceIdInfo = new GUIContent("Face Id", "The unique id for the face symbol.");
        static public GUIContent gameobjectTitleAndHint = new GUIContent("GameObject", "Container of all resources used from the behavior.");
        static public GUIContent behaviorInfo = new GUIContent("Behavior", "Describes the actions taken when symbol produced by this model is detected. Different actions can be defined by each detection phase and optionally you can set the gameobject on which the actions are executed.");
        static public GUIContent behaviorSettingsInfo = new GUIContent("Behavior Settings", "Additional options for more advanced usage.");
        static public GUIContent removeBindingsInfo = new GUIContent("Remove All Bindings", "This action will destroy all bindings.");
        static public GUIContent removeEventsInfo = new GUIContent("Remove All Events", "This action will remove all events.");
        static public GUIContent enableEventsInfo = new GUIContent("Enable Events", "Enables or disables event listening on this tracker.");
        static public GUIContent traceSymbolsInfo = new GUIContent("Trace Symbols", "Prints symbols information on the screen when running in the Editor.");
        static public GUIContent detectOnlyInfo = new GUIContent("Detect Only", "The tracker will only performs detection. Use this option if you don't require persistence among frames.");
        static public GUIContent enabledInfo = new GUIContent("Enabled", "Enables or disables the model inside the binding.");
        static public GUIContent dataInfo = new GUIContent("Data", "The encoded descriptors to be detected.");
        static public GUIContent barcodeTypeInfo = new GUIContent("Barcode Type", "The barcode encoding algorithm to be used.");
        static public GUIContent approxResolutionInfo = new GUIContent("Approx. Resolution", "The approximated resolution for the generated barcodes.");
        static public GUIContent buildWithColorsInfo = new GUIContent("Build with colors", "The barcode will be encoded using color info.");
        static public GUIContent autoOrientationInfo = new GUIContent("Auto Orientation", "Enable automatic view orientation on mobile devices.");
        static public GUIContent cameraPositionInfo = new GUIContent("Camera Position", "Allows to set the default camera sensor on mobile devices.");
        static public GUIContent cameraDeviceInfo = new GUIContent("Camera Device", "The list of all camera devices on the computer.");
        static public GUIContent inputSourceInfo = new GUIContent("Input Source", "Sets the input source type to use. You can also register your own input sources here.");
        static public GUIContent videoModeInfo = new GUIContent("Video Mode", "Common video resolution presets. You can also define customized parameters.");
        static public GUIContent inputAutoStarInfo = new GUIContent("Auto Start", "Starts the capture automatically when the component is started.");
        static public GUIContent inputOpenInfo = new GUIContent("Open", "Opens the selected device and starts capturing.");
        static public GUIContent inputCloseInfo = new GUIContent("Close", "Stops capturing and closes the selected device.");
        static public GUIContent faceDataInfo = new GUIContent("Face Data", "The training data for face detection and landmarks recognition.");
    }
}
