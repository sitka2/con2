﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Vexpot.Arcolib.CustomEditors
{
    public class DrawUtils
    {
        /// <summary>
        /// 
        /// </summary>
        static public Color separaratorColor = new Color(0.5f,0.5f,0.5f,0.4f);

        static public void ComputeRectWithAspectRatio(Rect rectangle, Rect into, ref Rect resultRect)
        {
            float width = rectangle.width;
            float height = rectangle.height;
            float factorX = into.width / width;
            float factorY = into.height / height;
            float factor = 1.0f;
            factor = factorX < factorY ? factorX : factorY;
            width *= factor;
            height *= factor;
            resultRect.Set(into.x + (into.width - width) / 2, into.y + (into.height - height) / 2, width, height);
        }

        static public void DrawCanvasGrid(Rect area, float spacing, Color color)
        {
            int cols = Mathf.CeilToInt(area.width / spacing);
            int rows = Mathf.CeilToInt(area.height / spacing);

            Handles.BeginGUI();
            Handles.color = color;
            Vector3 offset = new Vector3(area.x, area.y);

            for (int i = 0; i < cols; i++)
            {
                Handles.DrawLine(new Vector3(spacing * i, 0) + offset, new Vector3(spacing * i, area.height) + offset);
            }

            for (int j = 0; j < rows; j++)
            {
                Handles.DrawLine(new Vector3(0, spacing * j) + offset, new Vector3(area.width, spacing * j) + offset);
            }

            Handles.color = Color.white;
            Handles.EndGUI();
        }

        static public void DrawRating(Rect rect, string label, float quality, Texture2D image, Texture2D image2, int spacing = 18)
        {
            if (quality > 0)
            {
                EditorGUI.LabelField(rect, label);
                rect.x += 50;
                rect.y += 1;
                for (int i = 0; i < 5; i++)
                {
                    float value = (i / 4.0f) - 0.02f * i;            
                    if (value < quality)
                        GUI.Label(rect, image);
                    else
                        GUI.Label(rect, image2);
                    rect.x += spacing;
                }
            }
            else
            {
                EditorGUI.LabelField(rect, label + " Not Available");
            }
        }
    }

    public class ModelManager : EditorWindow
    {
        private enum ManagerMode
        {
            Extended,
            Compact
        }

        private BarcodeModelGenerator _barcodeGenerator = null;
        private FeaturesModelGenerator _featuresGenerator = null;
        static private Color _gridColor = new Color(0.2f, 0.2f, 0.2f, 0.2f);

        private string[] _menuOptions = new string[] { "Barcode", "Features" };

        private  int _currentGeneratorType = 0;
        private ManagerMode _currentMode = ManagerMode.Extended;
        private const int _hpadding = 3;
        private const int _vpadding = 46;
        private IModelGenerator _currentGeneratorInstance;

        [MenuItem("Tools/Arcolib/Model Manager 1.0")]
        public static void OpenModelManager()
        {
            ModelManager editorWindow = GetWindow<ModelManager>();
            editorWindow.minSize = new Vector2(1000, 650);
            editorWindow.titleContent = new GUIContent("Model Manager");
            editorWindow.Show();
        }

        /// <summary>
        /// 
        /// </summary>
        void OnEnable()
        {
            if (_barcodeGenerator == null)
                _barcodeGenerator = new BarcodeModelGenerator();

            if (_featuresGenerator == null)
                _featuresGenerator = new FeaturesModelGenerator();

            _barcodeGenerator.OnEnable(this);
            _featuresGenerator.OnEnable(this);
        }

        /// <summary>
        /// 
        /// </summary>
        void OnDestroy()
        {
            _barcodeGenerator.OnDestroy();
            _featuresGenerator.OnDestroy();
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnGUI()
        {
            EditorGUILayout.Separator();
    
            // toolbar
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            _currentGeneratorType = GUILayout.Toolbar(_currentGeneratorType, _menuOptions, GUILayout.MinHeight(30), GUILayout.MinWidth(300));
            GUILayout.FlexibleSpace();
            _currentMode = (ManagerMode)EditorGUILayout.EnumPopup(_currentMode, GUILayout.MaxWidth(80));
            EditorGUILayout.EndHorizontal();

            var size = this.position;

            switch (_currentGeneratorType)
            {
                case 0:
                    _currentGeneratorInstance = _barcodeGenerator;
                    break;
                case 1:
                    _currentGeneratorInstance = _featuresGenerator;
                    break;
            }

            EditorGUILayout.BeginHorizontal();

            ApplyMode(_currentMode);

            // preview left panel
            Rect rect = new Rect(_hpadding, _vpadding, size.width * 0.68f, size.height * 0.9f);

            if (_currentMode == ManagerMode.Extended)
            {
                EditorGUILayout.BeginVertical(GUILayout.MinWidth(rect.width + _hpadding));
                EditorGUILayout.Separator();             
                EditorGUI.DrawRect(rect, Color.grey);
                DrawUtils.DrawCanvasGrid(rect, 20, _gridColor);
                _currentGeneratorInstance.OnPreviewTexture(rect);
                //------
                EditorGUILayout.EndVertical();
            }

            // options right panel
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            _currentGeneratorInstance.OnGUI();
            EditorGUILayout.EndVertical();   

            EditorGUILayout.EndHorizontal();
        }

        private void ApplyMode(ManagerMode mode)
        {
            switch (mode)
            {
                case ManagerMode.Compact:
                     minSize = new Vector2(400, 650);
                    break;
                case ManagerMode.Extended:
                    minSize = new Vector2(1000, 650);
                    break;
            }                
        }        
    }
}
