﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class gety : MonoBehaviour {
    public GameObject cub,o,s8;
    public Slider sl;
    public GameObject ar, cam;
    public int s, v, k, d,n;
       public InputField input;

    public GameObject hand;
 
    // Use this for initialization
    void Start () {
        s = 45;
        v = -45;
        k = -270;
        d = 185;

	}
   public void arc()
    {
        ar.SetActive(false);
        cam.SetActive(true);
    }
	public void go()
    {
        ar.SetActive(true);
        cam.SetActive(false);
        hand = GameObject.Find("Checker");
        cub.transform.position = hand.transform.position;
        
    }
	// Update is called once per frame
    public void zp()
    {
        o.transform.position = new Vector3(o.transform.position.x, o.transform.position.y, o.transform.position.z+0.1f);
    }

    public void zm()
    {
        o.transform.position = new Vector3(o.transform.position.x, o.transform.position.y, o.transform.position.z -0.1f);
    }
    public void xp()
    {
        o.transform.position = new Vector3(o.transform.position.x + 0.1f, o.transform.position.y, o.transform.position.z );
    }

    public void xm()
    {
        o.transform.position = new Vector3(o.transform.position.x- 0.1f, o.transform.position.y, o.transform.position.z) ;
    }
    public void yp()
    {
        o.transform.position = new Vector3(o.transform.position.x, o.transform.position.y + 0.1f, o.transform.position.z);
    }

    public void ym()
    {
        o.transform.position = new Vector3(o.transform.position.x, o.transform.position.y - 0.1f, o.transform.position.z);
    }

    public void sp()
    {
        
        cub.transform.localScale = new Vector3(cub.transform.localScale.x+3*n, cub.transform.localScale.y+3*n, cub.transform.localScale.y + 3*n);
    }

    public void sm()
    {
        n = int.Parse(input.text);
        cub.transform.localScale = new Vector3(cub.transform.localScale.x - 3*n, cub.transform.localScale.y - 3*n, cub.transform.localScale.y - 3*n);
    }
    void Update()
    {
       

        // 
    }

    public void vsl()
    {
        cub.transform.localEulerAngles = new Vector3(cub.transform.rotation.x, v, cub.transform.rotation.z);
        cub.transform.localScale = new Vector3(300, 300, 300);
    }
    public void sssl()
    {
        cub.transform.localEulerAngles = new Vector3(cub.transform.rotation.x, s, cub.transform.rotation.z);
        cub.transform.localScale = new Vector3(300, 300, 300);
    }
    public void ksl()
    {
        cub.transform.localEulerAngles = new Vector3(cub.transform.rotation.x, k, cub.transform.rotation.z);
        cub.transform.localScale = new Vector3(300, 300, 300);
    }
    public void dsl()
    {
        cub.transform.localEulerAngles = new Vector3(cub.transform.rotation.x, d, cub.transform.rotation.z);
        cub.transform.localScale = new Vector3(300, 300, 300);
    }
    public void zsl()
    {
        cub.transform.localEulerAngles = new Vector3(cub.transform.rotation.x, 0, cub.transform.rotation.z);
        cub.transform.localScale = new Vector3(0, 0, 0);
    }
    public void sls()
    {
        cub.transform.localEulerAngles = new Vector3(cub.transform.rotation.x, sl.value, cub.transform.rotation.z);
    }
}
